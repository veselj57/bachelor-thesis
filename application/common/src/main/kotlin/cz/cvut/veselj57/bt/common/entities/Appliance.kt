package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.*
import org.litote.kmongo.Id
import org.litote.kmongo.newId


@Serializable
data class Appliance(
    @SerialName("_id")
    @ContextualSerialization
    var id: Id<Appliance>,
    var display_name: String,
    var appliance_interface: PowerInterface,
    val optimization_plans: MutableList<OptimizationPlan>,
    var current_optimization_plan: Int,
    var partial_power_plan: PowerPlan,
    var avg_kwh_consumption: Double? = null
) {

    companion object{
        fun getEmpty() = Appliance(newId(), "", PowerInterface.None, mutableListOf(), -1, PowerPlan.getEmpty(), null)
    }


    fun getCurrentOptimizationPlan():OptimizationPlan? = optimization_plans.getOrNull(current_optimization_plan)


    fun deleteOptimizationPlan(position: Int){
        current_optimization_plan -= 1
        optimization_plans.removeAt(position)
    }


}
