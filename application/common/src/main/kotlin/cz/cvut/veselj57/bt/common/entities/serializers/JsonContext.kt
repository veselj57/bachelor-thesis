package cz.cvut.veselj57.bt.common.entities.serializers

import cz.cvut.veselj57.bt.common.entities.PowerInterface
import cz.cvut.veselj57.bt.common.entities.Optimizer
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import org.litote.kmongo.Id
import org.litote.kmongo.id.WrappedObjectId
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

object JsonContext{
    val context = SerializersModule{
        contextual(WrappedObjectId::class, IDSerializer)
        contextual(LocalDate::class, LocalDateSerializer)
        contextual(LocalTime::class, LocalTimeSerializer)
        contextual(LocalDateTime::class, LocalDateTimeSerializer)
        contextual(Id::class, IdSerializer())
    }

    val default = Json(context = context)


    operator fun invoke() = default
}



