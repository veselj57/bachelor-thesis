package cz.cvut.veselj57.bt.common.entities.serializers

import kotlinx.serialization.*
import org.litote.kmongo.id.WrappedObjectId

object IDSerializer : KSerializer<WrappedObjectId<*>> {
    override val descriptor: SerialDescriptor = PrimitiveDescriptor("IDSerializer", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = WrappedObjectId<Any>(decoder.decodeString())


    override fun serialize(encoder: Encoder, value: WrappedObjectId<*>) {
        encoder.encodeString(value.toString())

    }
}