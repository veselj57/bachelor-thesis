package cz.cvut.veselj57.bt.common.extensions

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

infix fun LocalDate.until(date: LocalDate) = DateIterator(this, date.minusDays(1L), 1)


class DateIterator(
    startDate: LocalDate,
    val endDateInclusive: LocalDate,
    val stepDays: Long
) : Iterator<LocalDate> {

    private var currentDate = startDate

    override fun hasNext() = currentDate <= endDateInclusive

    override fun next(): LocalDate {
        val next = currentDate
        currentDate = currentDate.plusDays(stepDays)
        return next
    }
}




infix fun LocalDateTime.until(date: LocalDateTime) =  DatetimeRange(this, date, false)


data class DatetimeRange(
    override val start: LocalDateTime,
    override val endInclusive: LocalDateTime,
    val includeEnd: Boolean
): ClosedRange<LocalDateTime>{

    infix fun byMinutes(step: Int) = DatetimeIterator(start, endInclusive, step, includeEnd).withIndex()
}


class DatetimeIterator(
    startInclusive: LocalDateTime,
    val end: LocalDateTime,
    val step: Int,
    val includeEnd: Boolean
) : Iterator<LocalDateTime> {

    private var currentDate = startInclusive

    override fun hasNext() = currentDate < end || (includeEnd && currentDate == end)

    override fun next(): LocalDateTime {
        val next = currentDate
        currentDate = currentDate.plusMinutes(step.toLong())
        return next
    }
}


public val SIMPLE_TIME: DateTimeFormatter
    get()  = DateTimeFormatter.ofPattern("HH:mm");



