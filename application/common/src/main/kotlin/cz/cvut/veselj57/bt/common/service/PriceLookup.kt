package cz.cvut.veselj57.bt.common.service

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import java.lang.Exception
import java.time.LocalDateTime
import java.util.*

class PriceLookup {

    val prices: SortedMap<LocalDateTime, ElectricityPrice>

    constructor(_prices: List<ElectricityPrice>){
        prices = sortedMapOf()
    }

    constructor(map: SortedMap<LocalDateTime, ElectricityPrice>){
        prices = map
    }


    fun get(_from: LocalDateTime, _to: LocalDateTime): PriceLookup {
        val from = _from.toLocalDate().atTime(_from.hour, 0)
        val to = _to.toLocalDate().atTime(_to.hour, 0)


        val first = prices.firstKey()
        val last = prices.lastKey()

        if (from < first || to > last.plusHours(1))
            throw Exception("Missing price for range: <$_from, $_to>")

        val subMap = prices.subMap(from, to)

        return PriceLookup(subMap)
    }

    fun getPrices(from: LocalDateTime, to: LocalDateTime) = get(from, to).prices.values.toList()

    fun get(datetime: LocalDateTime) = prices[datetime.roundDownToQuoter()]

    fun contains(time: LocalDateTime) = prices.contains(time)
}