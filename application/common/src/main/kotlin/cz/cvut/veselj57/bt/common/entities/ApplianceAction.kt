package cz.cvut.veselj57.bt.common.entities

import cz.cvut.veselj57.bt.common.entities.serializers.LocalDateSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDateTime

enum class ApplianceAction {
    ON, OFF
}