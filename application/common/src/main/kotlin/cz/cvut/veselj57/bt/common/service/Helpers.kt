package cz.cvut.veselj57.bt.common.service

import java.time.LocalDateTime


fun iterator(
    from: LocalDateTime,  //Inclusive
    to: LocalDateTime,  //Exclusive
    minutes: Long
) = object: Iterator<Pair<Int, LocalDateTime>>{
    var index = 0
    var iter = from
    override fun hasNext() = iter.isBefore(to)
    override fun next() = Pair(index++, iter.also { iter = it.plusMinutes(minutes)  })
}
