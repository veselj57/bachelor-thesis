package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.Serializable
import java.time.LocalDate
import java.time.LocalDateTime


@Serializable
data class ElectricityPrice(
    @ContextualSerialization
    var time: LocalDateTime,
    val eur: Double,
    val czk: Double
)
