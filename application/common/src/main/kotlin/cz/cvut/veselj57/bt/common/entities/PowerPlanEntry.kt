package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.LocalTime

@Serializable
data class PowerPlanEntry(
    @ContextualSerialization
    val time: LocalDateTime,
    var action: ApplianceAction
)