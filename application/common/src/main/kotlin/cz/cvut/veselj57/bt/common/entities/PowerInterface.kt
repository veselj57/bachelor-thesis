package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.modules.SerializersModule

@Serializable
sealed class PowerInterface {


    @SerialName("local")
    @Serializable
    data class Local(var gpio: Int) : PowerInterface() {
        override fun toString() = "local-$gpio"
    }

    @SerialName("remote")
    @Serializable
    data class Remote(var ip: String, var port: String, var gpio: Int) : PowerInterface() {
        override fun toString() = "$ip:$port-$gpio"
    }

    @SerialName("not_set")
    @Serializable
    object None : PowerInterface(){
        override fun toString() = "None"
    }
}