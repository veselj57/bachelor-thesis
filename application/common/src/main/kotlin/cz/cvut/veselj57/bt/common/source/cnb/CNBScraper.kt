package cz.cvut.veselj57.bt.common.source.ote

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object CNBScraper {

    fun scrape_EUR_CZK(date: LocalDate): Double {
        try {
            val date = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))

            val url = "https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt?date=$date"

            val br = BufferedReader(InputStreamReader(URL(url).openStream()))

            var parseLine: String?
            while (br.readLine().also { parseLine = it } != null) {
                val parts = parseLine!!.split("|")
                if (parts[0] == "EMU"){
                    return parts.last().replace(",", ".").toDouble()
                }
            }
            br.close()
            throw CNBScrapeException()
        }catch (e: Exception){
            e.printStackTrace()
            throw CNBScrapeException()
        }
    }

}