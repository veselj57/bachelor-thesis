package cz.cvut.veselj57.bt.common.test

import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.common.service.iterator
import org.litote.kmongo.newId
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import kotlin.random.Random

import cz.cvut.veselj57.bt.common.extensions.until

object Seeder {

    fun getAppliances(
        from: LocalDateTime,
        to: LocalDateTime,
        count: Int,
        prices: MutableList<ElectricityPrice> = getHourPrices(from, to)
    ): MutableList<Appliance> {
        val list = mutableListOf<Appliance>()
        repeat(count){
            val appliance = Appliance(
                newId(),
                "Appliance - " + Random.nextInt(1, 20),
                PowerInterface.None,
                getOptimizationPlan(3),
                0,
                PowerPlan(
                    LocalDateTime.now(),
                    LocalDateTime.now(),
                    listOf()
                )
            )

            list.add(appliance)
        }
        return list
    }

    fun getOptimizationPlan(count: Int): MutableList<OptimizationPlan> {
        val plans = mutableListOf<OptimizationPlan>()

        repeat(count){
            val optimizers = mutableListOf(
                BlockOptimizer(LocalTime.of(5, 0), LocalTime.of(9, 45),2, 2),
                BlockOptimizer(LocalTime.of(12, 30), LocalTime.of(15, 45),2, 2),
                FixedOptimizer(LocalTime.of(20, 0), LocalTime.of(22, 15))
            )

            val plan = OptimizationPlan(
                "Winter - " + Random.nextInt(1, 20),
                LocalTime.of(0, 0),
                LocalTime.of(0, 0),
                optimizers
            )

            plans.add(plan)
        }

        return plans
    }

    fun getPowerPlan(
        from: LocalDateTime,
        to: LocalDateTime,
        prices: MutableList<ElectricityPrice> = getHourPrices(from, to)
    ) = getPowerPlanManager(from, to, prices).getPowerPlanEntries()

    fun getPowerPlanManager(
        from: LocalDateTime,
        to: LocalDateTime,
        prices: MutableList<ElectricityPrice> = getHourPrices(from, to)
    ): PowerPlanManager {
        val manager = PowerPlanManager(from, to, prices)

        val i = manager.indices

        repeat(16){
            // val builder =  PowerPlanBuilder(date, getDayPrices(1).first())

            val x = Random.nextInt(i.first, i.last-8)

            manager.set(x..x+8, ApplianceAction.ON)
        }

        return manager
    }


    fun getHourPrices(from: LocalDateTime, to: LocalDateTime): MutableList<ElectricityPrice> {
        val list = ArrayList<ElectricityPrice>((Duration.between(from, to).toMinutes()/15).toInt())

        iterator(from, to, 60).forEach {
            val price = Random.nextDouble(-42.0, 42.0)
            list.add(it.first, ElectricityPrice(it.second, price, price * 27))
        }

        return list
    }

    fun getHourPrices(from: LocalDateTime, to: LocalDateTime, prices: Array<Double>): MutableList<ElectricityPrice> {
        val list = ArrayList<ElectricityPrice>((Duration.between(from, to).toMinutes()/60).toInt())

        (from until to byMinutes 60).forEach {
            list.add(ElectricityPrice(it.value, prices[it.value.hour], prices[it.value.hour]))
        }

        return list
    }



}