package cz.cvut.veselj57.bt.common.source.ote

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import org.jsoup.Jsoup
import java.time.LocalDate
import java.time.ZoneId


object DailyMarketScraper {

    public fun scrapePrices(date: LocalDate, eur_czk: Double): List<ElectricityPrice> {
        try {
            val url = "https://www.ote-cr.cz/cs/kratkodobe-trhy/elektrina/denni-trh?date=$date"

            val page = Jsoup.connect(url).get()

            val rows = page.select("#content-core .bigtable")[1]!!.select("tbody tr").apply {
                removeAt(size-1)
            }

            val rawPrices = rows.map { it.child(1).html().replace(",", ".").toDouble() }

            val prices = mutableListOf<ElectricityPrice>()
            val utcDate =  date.atStartOfDay().atZone(ZoneId.of("Europe/Prague")).toOffsetDateTime().atZoneSameInstant(ZoneId.of("UCT"))

            for (hourPrice in rawPrices){
                prices.add(ElectricityPrice(utcDate.toLocalDateTime(), hourPrice, eur_czk) )
                utcDate.plusHours(1L)
            }

            return prices
        }catch (e: Exception){
            e.printStackTrace()
            throw OTEScrapeException()
        }
    }

}