package cz.cvut.veselj57.bt.common.entities.serializers

import com.github.jershell.kbson.ObjectIdSerializer
import kotlinx.serialization.*
import org.bson.types.ObjectId
import org.litote.kmongo.Id
import org.litote.kmongo.id.IdTransformer
import org.litote.kmongo.id.WrappedObjectId
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

object LocalTimeSerializer : KSerializer<LocalTime> {
    override val descriptor: SerialDescriptor = PrimitiveDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = LocalTime.parse(decoder.decodeString())

    override fun serialize(encoder: Encoder, value: LocalTime)  = encoder.encodeString(value.toString())
}

object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
    override val descriptor: SerialDescriptor = PrimitiveDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = LocalDateTime.parse(decoder.decodeString())

    override fun serialize(encoder: Encoder, value: LocalDateTime)  = encoder.encodeString(value.toString())
}


object LocalDateSerializer : KSerializer<LocalDate> {


    override val descriptor: SerialDescriptor = PrimitiveDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): LocalDate{
        return LocalDate.parse(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, value: LocalDate){
        encoder.encodeString(value.toString())
    }
}


class IdSerializer<T : Id<*>> : KSerializer<T> {

    override val descriptor: SerialDescriptor = PrimitiveDescriptor("IdSerializer", PrimitiveKind.STRING)

    @Suppress("UNCHECKED_CAST")
    override fun deserialize(decoder: Decoder): T = WrappedObjectId<T>(decoder.decodeString()) as T

    override fun serialize(encoder: Encoder, obj: T) {
        IdTransformer.unwrapId(obj).also {
            when (it) {
                is String -> encoder.encodeString(it)
                is ObjectId -> ObjectIdSerializer.serialize(encoder, it)
                else -> error("unsupported id type $obj")
            }
        }
    }

}