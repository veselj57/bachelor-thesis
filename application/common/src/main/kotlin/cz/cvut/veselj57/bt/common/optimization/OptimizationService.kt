package cz.cvut.veselj57.bt.common.optimization

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.entities.OptimizationPlan
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import java.time.LocalDate

class OptimizationService {

    /*suspend fun calcPowerPlanFor(
        appliances: List<Appliance>,
        date: LocalDate,
        prices: List<ElectricityPrice>
    ) = appliances.map {Pair(it.id, calcPowerPlan(it, date, prices)) }*/

    suspend fun calcPowerPlan(plan: OptimizationPlan, date: LocalDate, prices: List<ElectricityPrice>): PowerPlan {
        val manager = PowerPlanManager(plan.getFrom(date), plan.getFrom(date), prices)

        manager.applyOptimizers(plan.optimizers)

        return manager.getPowerPlan()
    }
}