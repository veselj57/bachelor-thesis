package cz.cvut.veselj57.bt.common.service

import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import java.lang.Exception
import java.lang.IndexOutOfBoundsException
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime

class PowerPlanManager private constructor(
    val from: LocalDateTime,    // Inclusive
    val to: LocalDateTime      // Exclusive
){

    val executions = (Duration.between(from, to).toMinutes()/15).toInt()

    val indices = getIndexRange(from, to)

    var actions = Array(executions){ ApplianceAction.OFF }

    var prices = Array(executions){ Double.NaN }


    constructor(
        plan: PowerPlan, prices: List<ElectricityPrice>
    ):this(
        plan.from, plan.to, prices, plan.plan
    )

    constructor(
        from: LocalDateTime,    // Inclusive
        to: LocalDateTime,      // Exclusive
        prices: Array<Double>,
        actions: Array<ApplianceAction>
    ):this(from, to){
        this.actions = actions
        this.prices = prices
    }

    constructor(
        from: LocalDateTime,
        to: LocalDateTime,
        prices:  List<ElectricityPrice>,
        actions: List<PowerPlanEntry> = listOf()
    ):this (from, to) {
        actions.filter { from <= it.time && it.time < to }.forEach {
            this.actions[getIndexOf(it.time)] = it.action
        }

        val lookup = prices.associateBy { it.time }

        for( i in indices){
            this.prices[i] = lookup[getTimeOf(i).withMinute(0)]?.czk ?: throw Exception("There is no price for : ${getTimeOf(i)}")
        }
    }

    fun getIndexOf(datetime: LocalDateTime) = (Duration.between(from, datetime).toMinutes()/15).toInt()

    fun getTimeOf(index: Int) = from.plusMinutes(index * 15L)

    fun getIndexRange(from: LocalDateTime, to: LocalDateTime) = getIndexOf(from) until getIndexOf(to)

    fun set(from: LocalDateTime, to: LocalDateTime, action: ApplianceAction){
        (getIndexOf(from) until getIndexOf(to)).forEach { actions[it] = action }
    }

    fun set(range: IntRange, action: ApplianceAction){
        range.forEach { actions[it] = action }
    }

    operator fun set(index: Int, action: ApplianceAction) { actions[index] = action }

    override fun toString() = "from: $from, to: $to \n" + cluster().joinToString("\n")

    fun contains(datetime: LocalDateTime) = from <= datetime && datetime < to

    fun getAction(time: LocalDateTime): ApplianceAction {
        return cluster().firstOrNull {
            it.contains(time)
        }?.action ?: throw Exception("Could not find interval block")
    }

    fun getPowerPlanEntries(range: IntRange = indices): List<PowerPlanEntry> {
        return actions.slice(range).mapIndexedNotNull { i, action ->
            if (action != ApplianceAction.OFF)
                PowerPlanEntry(from.plusMinutes(i * 15L), action)
            else
                null
        }.distinctBy { it.time }
    }

    fun setBy(manager: PowerPlanManager){
        set(getIndexRange(manager.from, manager.to), ApplianceAction.OFF)

        manager.getPowerPlanEntries().forEach {
            set(getIndexOf(it.time), it.action)
        }
    }

    fun getPowerPlanEntries(from: LocalDateTime, to: LocalDateTime) = getPowerPlanEntries(getIndexRange(from, to))

    fun getPowerPlan(
        from: LocalDateTime = this.from,
        to: LocalDateTime = this.to
    ): PowerPlan =
        PowerPlan(
            from,
            to,
            getPowerPlanEntries(getIndexRange(from, to))
        )

    fun getSubPowerPlanManager(from: LocalDateTime, to: LocalDateTime): PowerPlanManager{
        if ( !(this.from <= from && to < this.to)){
            throw IndexOutOfBoundsException("New PowerPlanManager is bigger then old one")
        }

        val range = getIndexRange(from, to)

        return PowerPlanManager(from, to, prices.sliceArray(range), actions.sliceArray(range) )
    }

    fun applyToPowerPlanEntries(plan: MutableList<PowerPlanEntry>) {
        plan.apply {
            removeAll { from <= it.time && it.time < to }
            addAll(getPowerPlanEntries())
        }
    }

    @Deprecated("", ReplaceWith("applyOptimizationPlan"))
    fun applyOptimizers(optimizers: List<Optimizer>){
        optimizers.forEach {
            it.applyToPowerPlan(from.toLocalDate(), this)
        }
    }

    fun applyOptimizationPlan(plan: OptimizationPlan, date: LocalDate){
        plan.optimizers.forEach {
            it.applyToPowerPlan(date, this)
        }
    }

    fun getEstimatedCost(consumption_kWh: Double, from: LocalDateTime = this.from, to: LocalDateTime = this.to): Double {
        var cost = 0.0

        for( i in getIndexRange(from, to)){
            if (actions[i] == ApplianceAction.ON) cost+= consumption_kWh * prices[i] / 4
        }
        return cost
    }

    fun getEstimated_kWh(consumption_kWh: Double, from: LocalDateTime = this.from, to: LocalDateTime = this.to): Double {
        var kWhs = 0.0

        for( i in getIndexRange(from, to)){
            if (actions[i] == ApplianceAction.ON) kWhs+= 0.25 * consumption_kWh
        }
        return kWhs
    }



    fun cluster(from: LocalDateTime = this.from, to: LocalDateTime = this.to): List<ActionInterval> {
        val range = getIndexOf(from)..getIndexOf(to)

        val clustered: MutableList<ActionInterval> = mutableListOf()

        var start = range.first
        for ( i in range.first+1..range.last){
            val end: Int = if (i == range.last)
                range.last
            else
                if (actions[i-1] != actions[i]) i else continue

            val item = ActionInterval(
                getTimeOf(start),
                getTimeOf(end),
                actions[end - 1]
            )
            item.calcMetrics(prices.slice(start until end))
            clustered.add(item)

            start = i
        }

        return clustered.filter { it.action == ApplianceAction.ON }
    }

}

fun LocalDateTime.roundDownToQuoter() = toLocalDate().atTime(hour, minute - minute%15)

fun LocalDateTime.roundDownToHour() = toLocalDate().atTime(hour, 0)
fun LocalDateTime.roundUpToHour() = toLocalDate().atTime(hour, 0).plusHours(1L)


