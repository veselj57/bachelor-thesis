@file:ContextualSerialization(LocalTime::class)

package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Serializable
data class OptimizationPlan(
    var name: String,
    val from: LocalTime,
    val to: LocalTime,
    val optimizers: List<Optimizer>
)

{

    fun getFrom(date: LocalDate) = date.atTime(from)

    fun getTo(date: LocalDate) = if (from < to) date.atTime(to) else date.plusDays(1).atTime(to)


    fun getDateTimeOf(date: LocalDate, time: LocalTime): LocalDateTime {
        return if( time > from )
            date.atTime(time)
        else
            date.plusDays(1).atTime(time)
    }


    fun isInTimeRange(time: LocalTime): Boolean {
        val day = LocalDate.now()

        val from = day.atTime(from)
        val datetime = time.atDate(day)
        val to = if (this.from < this.from) day.atTime(to) else day.plusDays(1).atTime(to)

        return from <= datetime && datetime < to
    }





}