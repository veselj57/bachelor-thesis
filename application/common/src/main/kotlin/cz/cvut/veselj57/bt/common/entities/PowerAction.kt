package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.Serializable

@Serializable
data class PowerAction(val port: PowerInterface, val action: ApplianceAction)