@file:ContextualSerialization(LocalTime::class)

package cz.cvut.veselj57.bt.common.entities

import cz.cvut.veselj57.bt.common.extensions.SIMPLE_TIME

import cz.cvut.veselj57.bt.common.extensions.toDec
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import kotlinx.serialization.*
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter


@Serializable
sealed class Optimizer{
    abstract val from: LocalTime

    abstract val to: LocalTime

    abstract fun applyToPowerPlan(date: LocalDate, plan: PowerPlanManager)

    fun getFrom(date: LocalDate) = date.atTime(from)

    fun getTo(date: LocalDate) = if (from < to) date.atTime(to) else date.plusDays(1).atTime(to)

    fun getDuration(): Int{
        val day = LocalDate.now()
        return Duration.between(getFrom(day), getTo(day)).toMinutes().toInt()/15
    }
    
    abstract fun getActiveHours():Double


    fun isTimeRangeWithingPlan(plan: OptimizationPlan) =  plan.isInTimeRange(from) && plan.isInTimeRange(to)

}


@Serializable
@SerialName("block_optimizer")
data class BlockOptimizer(
    override var from: LocalTime, // Inclusive
    override var to: LocalTime,  //Exclusive

    var block_length: Int,
    var block_count: Int
) : Optimizer() {

    companion object{
        val maxLength = 96
        val maxBlocks = 8
    }

    override fun applyToPowerPlan(date: LocalDate, plan: PowerPlanManager) {
        val interval = plan.getIndexRange(getFrom(date), getTo(date))

        val block = Array(block_count){
                i ->interval.first + i * block_length
        }
        val termination = Array(block_count){
                i -> interval.last - (block_count -i) * block_length+1
        }

        var bestCombination = Array(block_count){-1}
        var bestSum = Double.POSITIVE_INFINITY


        loop@while (true){
            var sum = 0.0
            for (i in block.indices){
                for (c in block[i] until block[i]+block_length){
                    sum += plan.prices[block[i]]
                }
            }

            if (sum < bestSum){
                bestSum = sum
                bestCombination = block.clone()
            }

            for ( i in block.indices){
                // Find which block is on termination stage
                if (block[i] == termination[i]){

                    // If first block is on termination stage
                    // all other blocks are on termination stage as well
                    if (i == 0) break@loop

                    // Move the block before
                    block[i-1]++

                    // Reset position of blocks
                    // after the block that was moved
                    for(x in i..block.indices.last)
                        block[x] =block[x-1] + block_length

                    continue@loop
                }
            }
            block[block.size-1]++
        }

        //Update power plan
        for (b in bestCombination.indices){
            for (i in bestCombination[b] until bestCombination[b]+block_length){
                plan[i] = ApplianceAction.ON
            }
        }
    }

    override fun getActiveHours() = block_count * (block_length.toDouble() / 4 )

    override fun toString() = "${from.format(SIMPLE_TIME)} – ${to.format(SIMPLE_TIME)}    ${block_count}x${(block_length.toDouble()/4).toDec()}h"
}


@Serializable
@SerialName("test_optimizer")
data class FixedOptimizer(
    override var from: LocalTime,
    override var to: LocalTime
) : Optimizer() {

    override fun applyToPowerPlan(date: LocalDate, plan: PowerPlanManager) {
        val indexes = plan.getIndexRange(getFrom(date), getTo(date))

        plan.set(indexes, ApplianceAction.ON)
    }

    override fun getActiveHours() = getDuration().toDouble()/4

    override fun toString() = "${from.format(SIMPLE_TIME)} – ${to.format(SIMPLE_TIME)}"
}

