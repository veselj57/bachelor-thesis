package cz.cvut.veselj57.bt.common.extensions

fun Double.toDec(numberOfDecimals: Int = 2): String = "%.${numberOfDecimals}f".format(this)