@file:ContextualSerialization(LocalDateTime::class)
package cz.cvut.veselj57.bt.common.entities

import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.time.LocalDate
import java.time.LocalDateTime

data class Metrics(var average: Double)

@Serializable
data class ActionInterval(
    var from: LocalDateTime, //inclusive
    var to: LocalDateTime, // exclusive

    val action: ApplianceAction = ApplianceAction.ON
){
    companion object{
        fun empty() = ActionInterval(LocalDate.now().atStartOfDay(), LocalDate.now().atStartOfDay())
    }

    @Transient
    lateinit var metrics: Metrics

    fun calcMetrics(list: List<Double>){
        metrics = Metrics(list.average())
    }


    fun contains(time: LocalDateTime) = from <= time && time < to
}