package cz.cvut.veselj57.bt.common.entities

import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import kotlinx.serialization.ContextualSerialization
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.lang.IndexOutOfBoundsException
import java.time.LocalDate
import java.time.LocalDateTime

@Serializable
data class PowerPlan(
    @ContextualSerialization
    val from: LocalDateTime,

    @ContextualSerialization
    val to: LocalDateTime,

    var plan: List<PowerPlanEntry> = mutableListOf()
){
    companion object{
        val defaultFrom = LocalDate.now().minusDays(2).atTime(0, 0)
        val defaultTo = LocalDate.now().plusDays(2).atTime(0, 0)

        fun getEmpty() = PowerPlan(
            defaultFrom,
            defaultTo,
            mutableListOf()
        )
    }


    @Transient
    lateinit var manager: PowerPlanManager

    fun updateFromManager(){
        plan = manager.getPowerPlanEntries(from, to).toMutableList()
    }

    suspend fun initManager(prices: List<ElectricityPrice>){
        manager = PowerPlanManager(this, prices)
    }

    fun getState(time: LocalDateTime): ApplianceAction {
        return plan.find {
            val start = it.time
            val end = it.time.plusMinutes(15L)

            start<= time && time < end
        }?.action ?: ApplianceAction.OFF
    }


    fun subPlan(from: LocalDateTime, to: LocalDateTime): PowerPlan {
        if ( !(this.from <= from && to < this.to)){
            throw IndexOutOfBoundsException("New PowerPlan is bigger then old one")
        }

        val items = plan.filter { from <= it.time && it.time < to }

        return PowerPlan(from, to, items)
    }


    fun replace(from: LocalDateTime, to: LocalDateTime, actions: List<PowerPlanEntry>): PowerPlan {
        val list = plan.toMutableList()

        list.removeAll { from <= it.time && it.time < to }
        list.addAll(actions)
        list.sortBy { it.time }

        return PowerPlan(from, to, plan)
    }
}