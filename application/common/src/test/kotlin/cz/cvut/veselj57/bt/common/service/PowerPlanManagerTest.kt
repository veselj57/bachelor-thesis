package cz.cvut.veselj57.bt.common.service

import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import org.junit.Assert.*
import org.junit.Test
import java.time.LocalDateTime

class PowerPlanManagerTest{

    @Test
    fun test(){
        val actions = Array(10){ApplianceAction.OFF}
        val prices = Array(10){10.0}

        val from = LocalDateTime.of(2020, 1, 1, 0, 0)
        val to = LocalDateTime.of(2020, 1, 1, 2, 30)

        val plan = PowerPlanManager(from, to,prices, actions)

        plan.set(from, to.minusMinutes(15L), ApplianceAction.ON)



        plan.actions.sliceArray(0..8).forEach {
            assertEquals(it, ApplianceAction.ON)
        }

        assertEquals(plan.actions[9], ApplianceAction.OFF)


        println("")


    }

}