
plugins {
    java
    kotlin("jvm") version "1.3.70"
    kotlin("plugin.serialization") version "1.3.70"
}

group = "cz.cvut.veselj57.bt"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")

    api("org.litote.kmongo:kmongo-serialization-mapping:4.0.0")
    api("org.litote.kmongo:kmongo-id:4.0.0")

    implementation("org.jsoup:jsoup:1.13.1")

    testImplementation("junit:junit:4.13")
}

tasks.test {
    useJUnit()

    maxHeapSize = "1G"
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}
