package cz.cvut.veselj57.bt.mobile_app.scenario.api


import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.mobile_app.network.APIAppliance
import java.time.LocalDateTime

open class AppliancesAPITest(val appliances: MutableList<Appliance>) :APIAppliance{


    override suspend fun getAppliances(): List<Appliance>  = appliances

    override suspend fun getAppliance(id: String, from: String?, to: String?) = appliances.first { it.id.toString() === id }.apply {
        val from = LocalDateTime.parse(from);
        val to = LocalDateTime.parse(to);

        partial_power_plan = PowerPlan(
            from,
            to,
            partial_power_plan.plan.filter { from <= it.time && it.time < to })

    }

    override suspend fun postAppliance(appliance: Appliance): Appliance {
        val old =  appliances.first { it.id.toString() === appliance.id.toString()  }
        val plan = old.partial_power_plan
        plan.replace(appliance.partial_power_plan.from, appliance.partial_power_plan.to, appliance.partial_power_plan.plan)

        appliances.remove(old)

        appliances.add(appliance.apply { partial_power_plan = plan })
        return appliance
    }

    override suspend fun deleteAppliance(id: String) {
        appliances.removeIf { it.id.toString() === id }
    }

}
