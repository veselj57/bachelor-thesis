package cz.cvut.veselj57.bt.mobile_app.scenario.api

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.extensions.iterateByMinutes
import cz.cvut.veselj57.bt.common.service.PriceLookup
import cz.cvut.veselj57.bt.mobile_app.network.APIElectricityPrices
import java.time.LocalDateTime
import kotlin.random.Random

open class ElectricityPriceAPITest: APIElectricityPrices {

    val prices = PriceLookup(listOf())

    override suspend fun getPrices(from: String, to: String): List<ElectricityPrice> {
        val from = LocalDateTime.parse(from)
        val to = LocalDateTime.parse(to)
        val x = prices.prices


        from.iterateByMinutes(to, 15).forEach {
            val eur = Random.nextDouble(-40.0, 40.0)
            x.putIfAbsent(it, ElectricityPrice(it, eur, eur * 25 ))
        }

        return prices.getPrices(from, to)
    }


}