package cz.cvut.veselj57.bt.mobile_app.scenario


import android.app.Application
import android.content.Context
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.FragmentAppliances
import org.junit.Test
import org.junit.runner.RunWith


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.runner.AndroidJUnitRunner
import cz.cvut.veselj57.bt.mobile_app.conf.ServerConf
import cz.cvut.veselj57.bt.mobile_app.network.APIAppliance
import cz.cvut.veselj57.bt.mobile_app.network.APIElectricityPrices
import cz.cvut.veselj57.bt.mobile_app.scenario.api.AppliancesAPITest
import cz.cvut.veselj57.bt.mobile_app.scenario.api.ElectricityPriceAPITest
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ApplianceRepository
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ElectricityPricesRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module


class TestApplication: Application() {

     val applicationModule = module {
        single { ServerConf.dev }
        single { AppliancesAPITest(mutableListOf()) }
        single { ElectricityPriceAPITest() }
        single { ElectricityPricesRepository( get() ) }
        single { ApplianceRepository( get(), get() ) }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(applicationModule)
        }
    }
}


class FakeRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, TestApplication::class.java.name, context)
    }
}


@RunWith(AndroidJUnit4::class)
class TitleScreenTest {

    @Test
    fun testNavigationToInGameScreen() {
        // Create a TestNavHostController
        val x = ApplicationProvider.getApplicationContext<Application>()


        val navController = TestNavHostController(x)
        navController.setGraph(R.navigation.mobile_navigation)

        // Create a graphical FragmentScenario for the TitleScreen
        val titleScenario = launchFragmentInContainer<FragmentAppliances>(null, R.style.AppTheme)

        // Set the NavController property on the fragment
        titleScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)

        }

        titleScenario




    }

}


