package cz.cvut.veselj57.bt.mobile_app.core

import android.app.Application
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.conf.ServerConf
import cz.cvut.veselj57.bt.mobile_app.network.APIAppliance
import cz.cvut.veselj57.bt.mobile_app.network.APIElectricityPrices
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ApplianceRepository
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ElectricityPricesRepository
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import org.koin.dsl.module

open class Application : Application(), KoinComponent{

    private val retrofit = retrofit2.Retrofit
        .Builder()
        .baseUrl(ServerConf.dev.toString())
        .client(OkHttpClient())
        .addConverterFactory(JsonContext.default.asConverterFactory( MediaType.get("application/json")))
        .build()

    private val applRep by inject<ApplianceRepository>()

    open val applicationModule = module {
        single { ServerConf.dev }
        single { retrofit.create(APIAppliance::class.java)}
        single { retrofit.create(APIElectricityPrices::class.java)}
        single { ElectricityPricesRepository( get() ) }
        single { ApplianceRepository( get(), get() ) }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@Application)
            modules(applicationModule)
        }

        applRep.loadAppliances()

    }

}