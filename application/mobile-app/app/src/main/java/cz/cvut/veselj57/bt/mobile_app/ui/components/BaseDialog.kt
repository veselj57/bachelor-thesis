package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment

open class BaseDialog<T : Any> : AppCompatDialogFragment(){

    var request = 0
        private set

    lateinit var callback: T

    lateinit var dContext: Context
    lateinit var dArgs: Bundle

    companion object {
        internal const val REQUEST_TAG =  "REQUEST"
        internal const val INITIAL_VALUE =  "INITIAL_VALUE"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (targetFragment != null)
            callback = targetFragment as T

        if (arguments == null)
            throw Exception("You need to set REQUEST_TAG")
        else{
            dArgs = requireArguments()
            request = dArgs.getInt(REQUEST_TAG, 0)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dContext = context
    }


}