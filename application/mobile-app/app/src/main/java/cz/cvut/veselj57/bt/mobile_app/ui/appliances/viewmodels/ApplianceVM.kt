package cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels

import androidx.lifecycle.*
import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ApplianceRepository
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ElectricityPricesRepository
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.Id
import org.litote.kmongo.id.StringId
import org.litote.kmongo.newId
import java.lang.Exception


class FragmentApplianceVMFactory( private val id: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ApplianceVM(
            StringId(id)
        ) as T
    }
}

class ApplianceVM(id : Id<Appliance>?) : ViewModel(), KoinComponent {
    enum class Mode{EDITING, INSPECTING, NEW, LOADING}

    val repAppliances by inject<ApplianceRepository>()
    val repPrices by inject<ElectricityPricesRepository>()


    var mode = Mode.LOADING
        set(value) {
            field = value
            ldMode.value = mode
        }
    val ldMode = MutableLiveData<Mode>(mode)


    val ldAppliance = MutableLiveData<Appliance>()
    lateinit var appliance : Appliance



    init {
        viewModelScope.launch {
            if(id != null){
                appliance = repAppliances.get(id)
                val plan = appliance.partial_power_plan

                plan.manager = PowerPlanManager(
                    plan.from,
                    plan.to,
                    repPrices.getPrices(plan.from, plan.to),
                    plan.plan
                )
                ldAppliance.postValue(appliance)
                mode = Mode.INSPECTING
            } else {
                appliance = Appliance(
                    newId(),
                    "",
                    PowerInterface.None,
                    mutableListOf(),
                    -1,
                    PowerPlan.getEmpty()
                )
                ldAppliance.postValue(appliance)
                mode = Mode.NEW
            }

        }

    }

    fun update(call: Appliance.()->Unit){
        call.invoke(appliance)
        ldAppliance.postValue(appliance)
    }

    fun changeMode() {
        when(mode){
            Mode.EDITING, Mode.NEW ->{
                mode = Mode.LOADING

                repAppliances.persist(appliance){
                    this.appliance = it
                    ldAppliance.value = it

                    mode = Mode.INSPECTING
                }
            }
            Mode.INSPECTING ->mode = Mode.EDITING
            Mode.LOADING -> throw Exception("Should be prevented in UI")
        }

        ldMode.value = mode
    }

    fun delete(action: (Boolean)->Unit) {
        mode = Mode.LOADING
        repAppliances.delete(appliance.id, action)
    }

}
