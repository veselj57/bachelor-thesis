package cz.cvut.veselj57.bt.mobile_app.conf



data class ServerConf(val host: String, val port: Int){

    companion object{
        val dev = ServerConf("192.168.1.12", 8080)
    }

    override fun toString() = "http://$host:$port"
}