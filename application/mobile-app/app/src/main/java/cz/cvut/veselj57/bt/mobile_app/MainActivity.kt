package cz.cvut.veselj57.bt.mobile_app

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ApplianceRepository
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    val applianceRepository by inject<ApplianceRepository>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        navView.setupWithNavController(navController)
    }

}
