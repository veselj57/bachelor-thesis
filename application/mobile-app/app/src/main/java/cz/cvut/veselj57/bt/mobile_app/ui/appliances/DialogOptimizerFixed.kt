package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import cz.cvut.veselj57.bt.common.entities.FixedOptimizer
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.OptimizationPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.components.DialogTimePicker
import kotlinx.android.synthetic.main.dialog_optimizer_block.view.*
import java.time.LocalTime
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.DialogOptimizerFixedVMFactory as Factory


class DialogOptimizerFixed: DialogFragment(), DialogTimePicker.Callback {

    companion object{
        const val FROM_TIME_REQUEST = 1
        const val TO_TIME_REQUEST = 2
    }

    private val args by navArgs<DialogOptimizerBlockArgs>()

    private val vmPlan by navGraphViewModels<OptimizationPlanVM>(R.id.fragment_optimization_plan)

    val vm by viewModels<DialogOptimizerFixedVM>{ Factory(args.planPosition, vmPlan) }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_optimizer_fixed, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.tv_from.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogTimePicker.showDialog(parentFragmentManager, this@DialogOptimizerFixed, vm.optimizer.from, FROM_TIME_REQUEST)
            }
        }

        view.tv_to.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogTimePicker.showDialog(parentFragmentManager, this@DialogOptimizerFixed, vm.optimizer.to, TO_TIME_REQUEST)
            }
        }

        
        view.d_btn_back.setOnClickListener {
            dismiss()
        }

        view.d_btn_delete.setOnClickListener {
            vmPlan.update {
                optimizers.removeAt(args.planPosition)
            }
            dismiss()
        }

        view.d_btn_edit.setOnClickListener {
            val optimizer = vm.optimizer
            val plan = vmPlan.plan

            if ( ! optimizer.isTimeRangeWithingPlan(plan)){
                Toast.makeText(context!!, "Časy přesahují optimalizační plán.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            vm.commit()

            dismiss()
        }


        vm.ld.observe(viewLifecycleOwner, Observer {
            view.tv_from.setText(it.from.localized())
            view.tv_to.setText(it.to.localized())
        })

    }

    override fun onTimeSet(time: LocalTime, request: Int): Boolean {
        if (time.minute%15 != 0){
            Toast.makeText(context, "Spuštění může začínat pouze každou čtvrt hodinu.", Toast.LENGTH_LONG).show()
            return false
        }

        when(request){
            FROM_TIME_REQUEST -> vm.update { from = time }
            TO_TIME_REQUEST -> vm.update { to = time }
        }
        return true
    }

}


class DialogOptimizerFixedVMFactory(val position: Int, val planVM: OptimizationPlanVM) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = DialogOptimizerFixedVM(position, planVM) as T
}

class DialogOptimizerFixedVM(val position: Int, val planVM: OptimizationPlanVM) : ViewModel(){

    enum class Mode{ CREATE, UPDATE }

    val mode = if (position == -1) Mode.CREATE else Mode.UPDATE

    val optimizer = when(mode){
        Mode.CREATE -> FixedOptimizer(LocalTime.of(0, 0), LocalTime.of(0, 0))
        Mode.UPDATE -> (planVM.plan.optimizers[position] as FixedOptimizer).copy()
    }

    val ld = MutableLiveData(optimizer)

    fun update( call: FixedOptimizer.() -> Unit){
        call.invoke(optimizer)
        ld.postValue(optimizer)
    }

    fun commit() = planVM.update {
        when(mode){
            Mode.CREATE -> optimizers.add(optimizer)
            Mode.UPDATE -> optimizers[position] = optimizer
        }
    }

}

