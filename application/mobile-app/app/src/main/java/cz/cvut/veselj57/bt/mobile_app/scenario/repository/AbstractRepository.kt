package cz.cvut.veselj57.bt.mobile_app.scenario.repository

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import java.lang.Exception


open class AbstractRepository(){

    inline fun <T> launch(crossinline call: suspend ()->T) = GlobalScope.launch(Dispatchers.IO){
        call.invoke()
    }

}

fun <T, X: AbstractRepository> X.live(
    scope: CoroutineScope = GlobalScope,
    x: X = this,
    t:T? = null,
    of: suspend X.()->T?
): MutableLiveData<T?> {
    val data = MutableLiveData<T?>(t)

    scope.launch(Dispatchers.IO) {
        val value = try {
            of.invoke(x)
        }catch (e: Exception){
            e.printStackTrace()
            null
        }

        data.postValue(value)
    }

    return data
}

