package cz.cvut.veselj57.bt.mobile_app.ui.components.swipe_list

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.mobile_app.R

open class ListViewSwipeCallback(
        open val context: Context,
        open val leftIcon: Int,
        open val rightIcon: Int,
        open val leftBackground: String = "#8BC34A",
        open val rightBackground: String = "#FF5722"
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        if (direction == ItemTouchHelper.LEFT)
            leftSwipe(viewHolder.adapterPosition)

        if (direction == ItemTouchHelper.RIGHT)
            rightSwipe(viewHolder.adapterPosition)
    }

    open fun leftSwipe(position: Int){}

    open fun rightSwipe(position: Int){}

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        val itemView = viewHolder.itemView
        val backgroundCornerOffset = 20 //so background is behind the rounded corners of itemView
        val icon: Drawable
        val background: ColorDrawable

        when {
            dX > 0 -> { // Swiping to the right
                icon = ContextCompat.getDrawable(context, leftIcon)!!
                icon.setBounds(
                        itemView.left + (itemView.height - icon.intrinsicHeight) /2,
                        itemView.top + (itemView.height - icon.intrinsicHeight) / 2,
                        itemView.left + (itemView.height - icon.intrinsicHeight) /2 + icon.intrinsicWidth,
                        itemView.top + (itemView.height - icon.intrinsicHeight) / 2 + icon.intrinsicHeight
                )
                background = ColorDrawable(Color.parseColor(leftBackground))
                background.setBounds(itemView.left, itemView.top,
                        itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom)
            }
            dX < 0 -> { // Swiping to the left
                icon = ContextCompat.getDrawable(context, rightIcon)!!
                icon.setBounds(
                        itemView.right - (itemView.height - icon.intrinsicHeight) / 2 - icon.intrinsicWidth,
                        itemView.top + (itemView.height - icon.intrinsicHeight) / 2,
                        itemView.right - (itemView.height - icon.intrinsicHeight) /2,
                        itemView.top + (itemView.height - icon.intrinsicHeight) / 2 + icon.intrinsicHeight
                )
                background = ColorDrawable(Color.parseColor(rightBackground))
                background.setBounds(itemView.right + dX.toInt() - backgroundCornerOffset,
                        itemView.top, itemView.right, itemView.bottom)
            }
            else -> { // view is unSwiped
                icon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_delete_24)!!
                background = ColorDrawable(Color.parseColor(leftBackground))
                background.setBounds(0, 0, 0, 0)
            }
        }
        background.draw(c)
        icon.draw(c)
    }
}
