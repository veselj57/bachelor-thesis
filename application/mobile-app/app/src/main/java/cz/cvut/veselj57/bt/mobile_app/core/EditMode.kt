package cz.cvut.veselj57.bt.mobile_app.core



enum class EditMode {
    INSPECT, EDIT, NEW
}