package cz.cvut.veselj57.bt.mobile_app.scenario.repository

import androidx.lifecycle.MutableLiveData
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.mobile_app.network.APIAppliance
import kotlinx.coroutines.*
import org.litote.kmongo.Id
import java.lang.Exception

class ApplianceRepository(
    val API_appliance: APIAppliance,
    val API_prices: ElectricityPricesRepository
) {

    val scope = CoroutineScope(Dispatchers.IO)

    private val ldAppliances = MutableLiveData<List<Appliance>>()

    private val appliances = ApplianceMap<Appliance>()


    fun loadAppliances() = scope.launch {
        val call_appliances = async { API_appliance.getAppliances()}
        val call_prices = async { API_prices.getPrices(PowerPlan.defaultFrom, PowerPlan.defaultTo)}
        val prices = call_prices.await()

        call_appliances.await().forEach {
            it.partial_power_plan.initManager(prices)
            appliances[it.id] = it
        }

        ldAppliances.postValue(appliances.values.toList())
    }

    fun get(id: Id<Appliance>) = appliances[id] ?: throw Exception("Appliance is not in list of appliances")


    fun liveAppliances() = ldAppliances


    fun persist(appliance: Appliance, done: (appliance: Appliance)->Unit)  = GlobalScope.launch {

        val new = API_appliance.postAppliance(appliance)

        appliances[new.id] = appliance

        withContext(Dispatchers.Main){
            done.invoke(appliance)
        }

        ldAppliances.postValue(appliances.values)
    }

    fun delete(id: Id<Appliance>, done: (Boolean)->Unit) =GlobalScope.launch {
        val contains = appliances.contains(id)

        if (contains)
            API_appliance.deleteAppliance(id.toString())

        withContext(Dispatchers.Main){
            done.invoke(contains)
        }

    }

}

class ApplianceMap<T>(){

    val map = hashMapOf<String, T>()

    val values: List<T> get() = map.values.toList()
    val entries: List<MutableMap.MutableEntry<String, T>> get() = map.entries.toList()


    fun getValue(id: Id<Appliance>) = map.getValue(id.toString())

    fun remove(id: Id<Appliance>) { map.remove(id.toString())}

    fun contains(id: Id<Appliance>) = map.containsKey(id.toString())

    operator fun set(id: Id<Appliance>, appliance: T){
        map[id.toString()] = appliance
    }

    operator fun set(id: String, appliance: T){
        map[id] = appliance
    }

    operator fun get(id: Id<Appliance>) = map[id.toString()]
    operator fun get(id: String) = map[id]
}



