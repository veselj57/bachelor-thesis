package cz.cvut.veselj57.bt.mobile_app.support

import org.litote.kmongo.Id
import org.litote.kmongo.id.StringId
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


fun LocalDateTime.localized() = format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))

fun LocalDate.localized() = format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))

fun LocalTime.localized() = format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))

fun <T> String.toId() = StringId<T>(this)




fun Double.toDec(numberOfDecimals: Int = 2): String = "%.${numberOfDecimals}f".format(this)