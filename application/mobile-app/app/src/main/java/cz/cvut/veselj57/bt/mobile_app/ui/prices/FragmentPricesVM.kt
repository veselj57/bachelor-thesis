package cz.cvut.veselj57.bt.mobile_app.ui.prices

import androidx.lifecycle.*
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ElectricityPricesRepository
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.LocalDate

class FragmentPricesVM : ViewModel(), KoinComponent {

    private val pricesRep  by inject<ElectricityPricesRepository>()


    val ldPrices = MutableLiveData<List<ElectricityPrice>>()
    val ldDate = MutableLiveData<LocalDate>()

    init {
        load(LocalDate.now())
    }

    fun load(date: LocalDate){
        viewModelScope.launch {
            val prices = pricesRep.getPrices(
                date.atStartOfDay(), date.atStartOfDay().plusDays(1L)
            )
            ldPrices.postValue(prices)
            ldDate.postValue(date)
        }
    }

    fun setDate(date: LocalDate) {
        load(date)
    }

}