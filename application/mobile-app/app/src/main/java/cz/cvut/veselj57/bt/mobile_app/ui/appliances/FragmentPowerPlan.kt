package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.ApplianceVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.OptimizationPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.PowerPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.PowerPlanVMVMFactory
import cz.cvut.veselj57.bt.mobile_app.ui.components.ActionIntervalAdapter
import cz.cvut.veselj57.bt.mobile_app.ui.components.ActionIntervalAdapterSwipeCallback
import kotlinx.android.synthetic.main.fragment_power_plan.*


class FragmentPowerPlan: Fragment() {

    val vmAppliance by navGraphViewModels<ApplianceVM>(R.id.navigation_appliance)

    val vm by navGraphViewModels<PowerPlanVM>(R.id.fragment_power_plan){ PowerPlanVMVMFactory(vmAppliance) }

    private lateinit var touchHelper: ItemTouchHelper
    private lateinit var adapter: ActionIntervalAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        touchHelper =  ItemTouchHelper(ActionIntervalAdapterSwipeCallback(context!!))
        adapter = ActionIntervalAdapter(findNavController(), true)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_power_plan, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_power_plan.apply {
            adapter = this@FragmentPowerPlan.adapter
            layoutManager = LinearLayoutManager(context)

            touchHelper.attachToRecyclerView(this)

        }

        toolbar.apply {
            setNavigationOnClickListener {
                findNavController().navigateUp()
            }
        }

        vm.ldManager.observe(viewLifecycleOwner, Observer {
            adapter.refreshWith(it.cluster())
            tv_from.text = it.from.localized()
            tv_to.text = it.to.localized()

        })

        view_add_interval.setOnClickListener {
            findNavController().navigate(
                FragmentPowerPlanDirections.editPowerInterval(
                    DialogPowerPlanActionIntervalVM.Mode.CREATE.toString(),
                    JsonContext.default.stringify(ActionInterval.serializer(), ActionInterval.empty())
                )
            )
        }

        action_btn.setOnClickListener {
            vm.commit()
            findNavController().navigateUp()
        }

    }

}

