package cz.cvut.veselj57.bt.mobile_app.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.appbar.MaterialToolbar
import cz.cvut.veselj57.bt.mobile_app.R
import kotlinx.android.synthetic.main.fragment_history.view.*


class HistoryFragment : Fragment() {

    private lateinit var historyViewModel: HistoryViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        historyViewModel =
                ViewModelProviders.of(this).get(HistoryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_history, container, false)
        val textView: TextView = root.findViewById(R.id.label)
        historyViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        val toolbar:MaterialToolbar = root.toolbar
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)


        return root
    }
}
