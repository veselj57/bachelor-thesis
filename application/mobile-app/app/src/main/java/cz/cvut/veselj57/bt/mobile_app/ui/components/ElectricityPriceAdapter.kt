package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.toDec
import kotlinx.android.synthetic.main.rv_price.view.*
import java.time.format.DateTimeFormatter

class ElectricityPriceAdapter() : RecyclerView.Adapter<ElectricityPriceAdapter.MyViewHolder>(){

    inner class MyViewHolder(
        val view: View,
        val from: TextView,
        val to: TextView,
        val price: TextView
    ) : RecyclerView.ViewHolder(view)

    private var items = listOf<ElectricityPrice>()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.rv_power_plan_entry, parent, false)
        return MyViewHolder(view, view.tv_from, view.tv_to, view.tv_name)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item =  items[position]

        val pattern = DateTimeFormatter.ofPattern("HH:mm")

        holder.from.text = item.time.format(pattern)
        holder.to.text = item.time.plusHours(1L).format(pattern)

        holder.price.text = "${item.czk.toDec(2)} Kč/kWh"
    }

    fun refreshWith(items: List<ElectricityPrice>){
        this.items = items
        notifyDataSetChanged()
    }
}
