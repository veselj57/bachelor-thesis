package cz.cvut.veselj57.bt.mobile_app.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.conf.ServerConf
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * OpenStreetMapAPI mapper
 */
interface APIElectricityPrices {


    @GET("/electricity-prices")
    suspend fun getPrices(
        @Query("from") from: String,
        @Query("to") to: String
    ): List<ElectricityPrice>




}
