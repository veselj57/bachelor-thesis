package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.common.entities.ActionInterval
import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.toDec
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.DialogPowerPlanActionIntervalVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.FragmentPowerPlan
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.FragmentPowerPlanDirections
import cz.cvut.veselj57.bt.mobile_app.ui.components.swipe_list.ListViewSwipeCallback
import kotlinx.android.synthetic.main.rv_power_plan_entry.view.*
import java.time.format.DateTimeFormatter

class ActionIntervalAdapter(val nav: NavController, val editable: Boolean = false) : RecyclerView.Adapter<ActionIntervalAdapter.MyViewHolder>(){


    inner class MyViewHolder(
        val view: View,
        val from_time: TextView,
        val from_date: TextView,
        val to: TextView,
        val price: TextView
    ) : RecyclerView.ViewHolder(view)

    private var items = listOf<ActionInterval>()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.rv_power_plan_entry, parent, false)
        return MyViewHolder(view, view.tv_from,view.tv_from_date, view.tv_to, view.tv_name)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item =  items[position]
        holder.from_time.text = item.from.format(DateTimeFormatter.ofPattern("HH:mm "))
        holder.from_date.text = item.from.format(DateTimeFormatter.ofPattern("dd. MM."))
        holder.to.text = item.to.format(DateTimeFormatter.ofPattern("HH:mm "))

        holder.price.text = "${item.metrics.average.toDec()} Kč/kWh"


        if (editable){
            holder.view.setOnClickListener {
                nav.navigate(FragmentPowerPlanDirections.editPowerInterval(
                    DialogPowerPlanActionIntervalVM.Mode.UPDATE.toString(),
                    JsonContext.default.stringify(ActionInterval.serializer(), item)
                ))
            }
        }

    }

    fun refreshWith(items: List<ActionInterval>){
        this.items = items
        notifyDataSetChanged()
    }
}

open class ActionIntervalAdapterSwipeCallback(
    context: Context,
    leftIcon: Int = R.drawable.ic_baseline_delete_24,
    rightIcon: Int = R.drawable.ic_baseline_delete_24,
    leftBackground: String = "#8BC34A",
    rightBackground: String = "#8BC34A"
) : ListViewSwipeCallback(context, leftIcon, rightIcon, leftBackground, rightBackground)
