package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.OptimizationPlan
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.ApplianceVM
import kotlinx.android.synthetic.main.dialog_optimization_plans.view.*
import kotlinx.android.synthetic.main.rv_optimizer_selection.view.*
import org.litote.kmongo.Id
import org.litote.kmongo.toId


class DialogOptimizationPlans: DialogFragment() {

    val args: DialogOptimizationPlansArgs by navArgs()

    private val vm by navGraphViewModels<ApplianceVM>(R.id.navigation_appliance)

    val adapter by lazy { PlanAdapter(args.applianceId.toId(), context!!, findNavController()) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_optimization_plans, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        vm.ldAppliance.observe(viewLifecycleOwner, Observer {
            adapter.refreshWith(it.optimization_plans, it.current_optimization_plan)
        })


        view.rv_optimization_plan.apply {
            adapter = this@DialogOptimizationPlans.adapter
            layoutManager = LinearLayoutManager(this@DialogOptimizationPlans.context)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL
                ).apply { setDrawable(ContextCompat.getDrawable(context, R.drawable.spacer)!!)  }
            )
        }
        

        view.d_btn_edit.setOnClickListener{
            val selected = adapter.items.indexOfFirst { it.selected }

            vm.update { this.current_optimization_plan = selected }

            dismiss()
        }

        view.view_add_optimizer.setOnClickListener {
            findNavController().navigate(
                DialogOptimizationPlansDirections.editPlan()
            )
        }

    }

}

class PlanAdapter(val id: Id<Appliance>, val context: Context, val nav: NavController) : RecyclerView.Adapter<PlanAdapter.MyViewHolder>(){

    lateinit var mRecyclerView: RecyclerView

    data class Item(var selected: Boolean, var plan: OptimizationPlan)

    inner class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val rb_selected:RadioButton = view.rb_plan_selected
        val btn_edit:ImageButton = view.btn_edit
        val tv_name:TextView = view.tv_name

        fun bind(item: Item, position: Int){
            rb_selected.isChecked = item.selected
            tv_name.text = item.plan.name
            rb_selected.setOnClickListener {
                val old = items.indexOfFirst { it.selected }

                items.forEach { it.selected = false }
                items.find { it.plan == item.plan }?.selected = true

                if (!mRecyclerView.isComputingLayout){
                    notifyItemChanged(old)
                    notifyItemChanged(position)
                }

            }

            btn_edit.setOnClickListener {
                nav.navigate(
                    DialogOptimizationPlansDirections.editPlan(id.toString(), position)
                )
            }
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    var items = listOf<Item>()

    override fun getItemCount() = items.size

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanAdapter.MyViewHolder {
        val view = LayoutInflater
            .from(context)
            .inflate(R.layout.rv_optimizer_selection, parent, false)
        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    fun refreshWith(items: List<OptimizationPlan>, selected: Int){
        this.items = items.mapIndexed { i, x -> Item(i == selected, x) }
        notifyDataSetChanged()
    }


}