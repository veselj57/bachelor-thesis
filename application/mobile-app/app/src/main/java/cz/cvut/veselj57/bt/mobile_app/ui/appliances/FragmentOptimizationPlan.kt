package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.ApplianceVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.OptimizationPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.components.OptimizersAdapter
import kotlinx.android.synthetic.main.dialog_optimization_plans.view.rv_optimization_plan
import kotlinx.android.synthetic.main.fragment_optimization_plan.*
import kotlinx.android.synthetic.main.fragment_optimization_plan.view.*
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.FragmentOptimizationPlanVMFactory as Factory


class FragmentOptimizationPlan: Fragment() {

    val args by navArgs<FragmentOptimizationPlanArgs>()

    val vmAppliance by navGraphViewModels<ApplianceVM>(R.id.navigation_appliance)

    val vm by navGraphViewModels<OptimizationPlanVM>(R.id.fragment_optimization_plan){ Factory(args.planPosition, vmAppliance) }

    lateinit var adapter: OptimizersAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = OptimizersAdapter(findNavController())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_optimization_plan, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.rv_optimization_plan.apply {
            adapter = this@FragmentOptimizationPlan.adapter
            layoutManager = LinearLayoutManager(context)
        }

        toolbar.apply {

            setNavigationOnClickListener {
                findNavController().navigateUp()
            }

            setOnMenuItemClickListener {
                vm.delete()
                findNavController().navigateUp()
                return@setOnMenuItemClickListener true
            }
        }

        vm.ldPlan.observe(viewLifecycleOwner, Observer {
            adapter.refreshWith(it.optimizers)
            view.tv_from.setText( it.from.localized())
            view.tv_to.setText( it.from.localized())

            if (view.tv_name.text.toString().isBlank())
                view.tv_name.setText(it.name)
        })


        view_add_optimizer.setOnClickListener {
            findNavController().navigate(
                FragmentOptimizationPlanDirections.newOptimizer()
            )
        }

        action_btn.setOnClickListener {

            val name = view.tv_name.text.toString()
            if (name.isBlank()){
                Toast.makeText(context!!, "Jméno je prázdné.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            vm.plan.optimizers.forEach {
                if ( ! it.isTimeRangeWithingPlan(vm.plan) ){
                    Toast.makeText(context!!, "Některá strategie přesahuje optimalizační plán", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }

            vm.update {
                this.name  = name
            }

            vm.commit()
            findNavController().navigateUp()
        }



    }

}

