package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import cz.cvut.veselj57.bt.common.entities.BlockOptimizer
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.OptimizationPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.components.DialogTimePicker
import kotlinx.android.synthetic.main.dialog_optimizer_block.view.*
import java.time.LocalTime
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.DialogOptimizerBlockVMFactory as Factory


class DialogOptimizerBlock: DialogFragment(), DialogTimePicker.Callback {

    companion object{
        const val FROM_TIME_REQUEST = 1
        const val TO_TIME_REQUEST = 2
    }

    private val args by navArgs<DialogOptimizerBlockArgs>()

    private val vmPlan by navGraphViewModels<OptimizationPlanVM>(R.id.fragment_optimization_plan)

    val vm by viewModels<DialogOptimizerBlockVM>{ Factory(args.planPosition, vmPlan) }

    lateinit var blockLengthAdapter: ArrayAdapter<String>
    lateinit var blockCountAdapter: ArrayAdapter<Int>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val layout  =  android.R.layout.simple_dropdown_item_1line

        blockCountAdapter = ArrayAdapter(context!!, layout, vm.genBlockCountStrings())

        blockLengthAdapter = ArrayAdapter(context!!,  layout,  vm.genBlockLengthStrings())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_optimizer_block, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.tv_from.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogTimePicker.showDialog(parentFragmentManager, this@DialogOptimizerBlock, vm.optimizer.from, FROM_TIME_REQUEST)
            }
        }

        view.tv_to.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogTimePicker.showDialog(parentFragmentManager, this@DialogOptimizerBlock, vm.optimizer.to, TO_TIME_REQUEST)
            }
        }

        view.tv_blocks.apply {
            setAdapter(blockCountAdapter)
            inputType = InputType.TYPE_NULL

            setOnItemClickListener { parent, view, position, id -> vm.update { this.block_count = position + 1 } }
        }

        view.tv_block_length.apply {
            setAdapter( blockLengthAdapter)
            inputType = InputType.TYPE_NULL

            setOnItemClickListener { parent, view, position, id -> vm.update { this.block_length = position +1} }
        }

        view.d_btn_back.setOnClickListener {
            dismiss()
        }

        view.d_btn_delete.setOnClickListener {
            vmPlan.update {
                optimizers.removeAt(args.planPosition)
            }
            dismiss()
        }

        view.d_btn_edit.setOnClickListener {

            val optimizer = vm.optimizer
            val plan = vmPlan.plan


            if ( !optimizer.isTimeRangeWithingPlan(plan)){
                Toast.makeText(context!!, "Časy přesahují optimalizační plán.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if ( optimizer.block_count * optimizer.block_length >= optimizer.getDuration()){
                Toast.makeText(context!!, "Celková délka bloků je příliš velké.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            vm.commit()

            dismiss()
        }


        vm.ld.observe(viewLifecycleOwner, Observer {
            view.tv_from.setText(it.from.localized())
            view.tv_to.setText(it.to.localized())

            view.tv_blocks.setText(it.block_count.toString(), false)

            view.tv_block_length.setText(vm.blockLengthToString(it.block_length), false)
        })

    }

    override fun onTimeSet(time: LocalTime, request: Int): Boolean {
        if (time.minute%15 != 0){
            Toast.makeText(context, "Spuštění může začínat pouze každou čtvrt hodinu.", Toast.LENGTH_LONG).show()
            return false
        }

        when(request){
            FROM_TIME_REQUEST -> vm.update { from = time }
            TO_TIME_REQUEST -> vm.update { to = time }
        }
        return true
    }

}


class DialogOptimizerBlockVMFactory(val position: Int, val planVM: OptimizationPlanVM) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = DialogOptimizerBlockVM(position, planVM) as T
}

class DialogOptimizerBlockVM(val position: Int, val planVM: OptimizationPlanVM) : ViewModel(){

    enum class Mode{ CREATE, UPDATE }

    val mode = if (position == -1) Mode.CREATE else Mode.UPDATE

    val optimizer = when(mode){
        Mode.CREATE -> BlockOptimizer(LocalTime.of(0, 0), LocalTime.of(0, 0), 4, 2)
        Mode.UPDATE -> (planVM.plan.optimizers[position] as BlockOptimizer).copy()
    }

    val ld = MutableLiveData(optimizer)

    fun update( call: BlockOptimizer.() -> Unit){
        call.invoke(optimizer)
        ld.postValue(optimizer)
    }


    fun commit() = planVM.update {
        when(mode){
            Mode.CREATE -> optimizers.add(optimizer)
            Mode.UPDATE -> optimizers[position] = optimizer
        }
    }


    fun blockLengthToString(length: Int) = "${length/4}h ${length%4*15}m"

    fun genBlockLengthStrings() = Array(BlockOptimizer.maxLength){ i -> blockLengthToString(i+1)}

    fun genBlockCountStrings() = (1..BlockOptimizer.maxLength).toList().toTypedArray()

}

