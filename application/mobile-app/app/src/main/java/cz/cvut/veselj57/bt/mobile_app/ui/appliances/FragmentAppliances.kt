package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import cz.cvut.veselj57.bt.mobile_app.R
import kotlinx.android.synthetic.main.rv_appliances.view.*
import java.time.LocalDateTime

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.veselj57.bt.mobile_app.core.BaseFragment
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.AppliancesVM
import kotlinx.android.synthetic.main.fragment_appliances.*
import kotlinx.android.synthetic.main.fragment_appliances.view.*


class FragmentAppliances : BaseFragment() {

    private val vm by viewModels<AppliancesVM>()

    private val nav by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val adapter = AppliancesAdapter()


        view.rv_appliances.apply {
            this.adapter = adapter
            layoutManager = LinearLayoutManager(fContext)
        }

        vm.appliances.observe(viewLifecycleOwner, Observer {
            if (it == null){
                loading_bar.visibility = View.VISIBLE
            }else{
                loading_bar.visibility = View.GONE
                adapter.refreshWith(it)
            }
        })

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    )= inflater.inflate(R.layout.fragment_appliances, container, false)



    inner class AppliancesAdapter : RecyclerView.Adapter<AppliancesAdapter.MyViewHolder>(){

        var appliance = listOf<Appliance>()

        inner class MyViewHolder(
            val view: View,
            val name: TextView,
            val state: TextView,
            val btn_action: ImageButton
        ) : RecyclerView.ViewHolder(view)


        override fun getItemCount() = appliance.size

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view: View = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.rv_appliances, parent, false)
            return MyViewHolder(view, view.tv_name, view.tv_state, view.btn_action)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val item = appliance[position]

            holder.name.text = item.display_name

            val state = item.partial_power_plan.getState(LocalDateTime.now())

            holder.state.text = state.toString()


            when(state){
                ApplianceAction.ON ->{
                    holder.btn_action.apply {
                        setBackgroundColor( Color.parseColor("#f44336"))
                    }
                }
                ApplianceAction.OFF -> {
                    holder.btn_action.apply {
                        setBackgroundColor( Color.parseColor("#8BC34A"))
                    }
                }
            }

            holder.state.text = state.toString()


            holder.view.setOnClickListener {

                findNavController().navigate(
                    FragmentAppliancesDirections.appliance(item.id.toString())
                )

            }
        }

        fun refreshWith(appliance: List<Appliance>){
            this.appliance = appliance
            notifyDataSetChanged()
        }
    }

}




