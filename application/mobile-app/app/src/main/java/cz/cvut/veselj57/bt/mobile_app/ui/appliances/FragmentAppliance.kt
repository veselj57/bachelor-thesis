package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import cz.cvut.veselj57.bt.mobile_app.R

import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputLayout
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.mobile_app.core.BaseFragment
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.ApplianceVM
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.FragmentApplianceVMFactory
import cz.cvut.veselj57.bt.mobile_app.ui.components.ActionIntervalAdapter
import cz.cvut.veselj57.bt.mobile_app.ui.components.OptimizersAdapter
import kotlinx.android.synthetic.main.fragment_appliance.*
import kotlinx.android.synthetic.main.fragment_appliance.action_btn
import kotlinx.android.synthetic.main.fragment_appliance.rv_optimization_plan
import kotlinx.android.synthetic.main.fragment_appliance.toolbar
import java.time.LocalDateTime


class FragmentAppliance : BaseFragment() {

    val args by navArgs<FragmentApplianceArgs>()


    private val vm by navGraphViewModels<ApplianceVM>(R.id.navigation_appliance){
        FragmentApplianceVMFactory(args.applianceId)
    }

    lateinit var powerAdapter: ActionIntervalAdapter
    lateinit var optimizationAdapter: OptimizersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        optimizationAdapter = OptimizersAdapter(findNavController())
        powerAdapter = ActionIntervalAdapter(findNavController())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.apply {
            setOnMenuItemClickListener {
                when(it.itemId){
                    R.id.menu_delete -> vm.delete{
                        findNavController().navigateUp()
                    }
                    else -> throw Exception ("Action not defined")
                }
                return@setOnMenuItemClickListener true
            }

            setNavigationOnClickListener {
                findNavController().navigateUp()
            }
        }

        rv_power_plan.adapter = powerAdapter
        rv_power_plan.layoutManager = LinearLayoutManager(fContext)


        rv_optimization_plan.adapter = optimizationAdapter
        rv_optimization_plan.layoutManager = LinearLayoutManager(fContext)

        vm.ldMode.observe(viewLifecycleOwner, Observer {
            when(it){
                ApplianceVM.Mode.EDITING, ApplianceVM.Mode.NEW -> {
                    enable(true)
                    action_btn.isEnabled = true

                    action_btn.setImageDrawable(ContextCompat.getDrawable(fContext, R.drawable.ic_baseline_save_24))
                    btn_power_plan.visibility = View.VISIBLE
                    btn_optimization_plan.visibility = View.VISIBLE

                    et_name.isEnabled = true
                    et_power_interface_layout.endIconMode= TextInputLayout.END_ICON_CUSTOM

                    tv_optimalization_name.visibility = View.VISIBLE

                }
                ApplianceVM.Mode.INSPECTING -> {
                    enable(false)
                    action_btn.isEnabled = true

                    action_btn.setImageDrawable(ContextCompat.getDrawable(fContext, R.drawable.ic_baseline_edit_24))
                    btn_power_plan.visibility = View.INVISIBLE
                    btn_optimization_plan.visibility = View.INVISIBLE

                    et_name.isEnabled = false
                    et_power_interface_layout.endIconMode = TextInputLayout.END_ICON_NONE
                    et_power_interface_layout.endIconDrawable = ContextCompat.getDrawable(fContext, R.drawable.ic_baseline_edit_24)

                    tv_optimalization_name.visibility = View.INVISIBLE
                }
                ApplianceVM.Mode.LOADING ->{
                    action_btn.isEnabled = false
                    enable(false)
                }
            }
        })

        vm.ldAppliance.observe(viewLifecycleOwner, Observer {
            et_name.setText(it.display_name)
            et_power_interface.setText( it.appliance_interface.toString() )
            powerAdapter.refreshWith(it.partial_power_plan.manager.cluster(LocalDateTime.now(), PowerPlan.defaultTo))

            val plan = it.getCurrentOptimizationPlan()

            if (plan == null){
                optimizationAdapter.refreshWith(listOf())
                tv_optimalization_name.text = "Bez plánu"
            }else{
                optimizationAdapter.refreshWith(plan.optimizers)
                tv_optimalization_name.text = plan.name
            }

        })

        action_btn.setOnClickListener {
            vm.update {
                display_name = et_name.text.toString()
            }
            vm.changeMode()
        }

        btn_power_plan.setOnClickListener {
            findNavController().navigate(FragmentApplianceDirections.dialogPowerPlan())
        }

        btn_optimization_plan.setOnClickListener {
            findNavController().navigate(FragmentApplianceDirections.dialogOptimizationPlans(args.applianceId))
        }



    }

    fun enable(enable: Boolean){
        btn_power_plan.isEnabled = enable
        btn_optimization_plan.isEnabled = enable

        et_name_layout.isEnabled = enable
        et_power_interface_layout.isEnabled = enable
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    )= inflater.inflate(R.layout.fragment_appliance, container, false)


}




