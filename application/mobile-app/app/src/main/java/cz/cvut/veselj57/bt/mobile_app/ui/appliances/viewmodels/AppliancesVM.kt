package cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels

import androidx.lifecycle.ViewModel
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ApplianceRepository
import org.koin.core.KoinComponent
import org.koin.core.inject


class AppliancesVM : ViewModel(), KoinComponent {

    val rep by inject<ApplianceRepository>()

    val appliances = rep.liveAppliances()

}
