package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import cz.cvut.veselj57.bt.common.entities.ActionInterval
import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.PowerPlanVM
import cz.cvut.veselj57.bt.mobile_app.ui.components.DialogDateTimePicker
import cz.cvut.veselj57.bt.mobile_app.ui.components.DialogTimePicker
import kotlinx.android.synthetic.main.dialog_optimizer_block.view.*
import kotlinx.android.synthetic.main.dialog_optimizer_block.view.toolbar
import kotlinx.android.synthetic.main.fragment_prices.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime


class DialogPowerPlanActionInterval: DialogFragment(), DialogDateTimePicker.Callback {

    companion object{
        const val FROM_TIME_REQUEST = 1
        const val TO_TIME_REQUEST = 2
    }

    private val args by navArgs<DialogPowerPlanActionIntervalArgs>()

    private val vmPlan by navGraphViewModels<PowerPlanVM>(R.id.fragment_power_plan)

    val vm by viewModels<DialogPowerPlanActionIntervalVM> {
        val interval = JsonContext.default.parse(ActionInterval.serializer(), args.interval)

        DialogPowerPlanActionIntervalVMFactory(vmPlan, interval, DialogPowerPlanActionIntervalVM.Mode.valueOf(args.mode))
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_power_plan_action_interval, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.tv_from.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogDateTimePicker(context, this@DialogPowerPlanActionInterval, vm.interval.from, FROM_TIME_REQUEST).show()
            }
        }

        view.tv_to.apply {
            inputType = InputType.TYPE_NULL

            setOnClickListener {
                DialogDateTimePicker(context, this@DialogPowerPlanActionInterval, vm.interval.to, TO_TIME_REQUEST).show()
            }
        }

        
        view.d_btn_back.setOnClickListener {
            dismiss()
        }

        view.d_btn_delete.setOnClickListener {
            vm.delete()
            dismiss()
        }

        view.d_btn_edit.setOnClickListener {
            if(vm.interval.from.isAfter(vm.interval.to)){
                Toast.makeText(context, "Začátek je před koncem", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            vm.commit()
            dismiss()
        }


        vm.ld.observe(viewLifecycleOwner, Observer {
            view.tv_from.setText(it.from.localized())
            view.tv_to.setText(it.to.localized())
        })

    }

    override fun onDateTimeSet(time: LocalDateTime, request: Int): Boolean {
        if (time.minute%15 != 0){
            Toast.makeText(context, "Spuštění může začínat pouze každou čtvrt hodinu.", Toast.LENGTH_LONG).show()
            return false
        }

        if (!vmPlan.manager.contains(time)){
            Toast.makeText(context, "Čas se nachází mimo povolené pásmo", Toast.LENGTH_LONG).show()
            return false
        }


        when(request){
            FROM_TIME_REQUEST -> vm.update { from = time }
            TO_TIME_REQUEST -> vm.update { to = time }
        }
        return true
    }

}


class DialogPowerPlanActionIntervalVMFactory(
    val planVM: PowerPlanVM,
    val old: ActionInterval,
    val mode: DialogPowerPlanActionIntervalVM.Mode
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = DialogPowerPlanActionIntervalVM(planVM, old, mode) as T
}

class DialogPowerPlanActionIntervalVM(
    val planVM: PowerPlanVM,
    val old: ActionInterval,
    val mode: Mode
) : ViewModel(){
    enum class Mode{CREATE, UPDATE}

    val interval = old.copy()

    val ld = MutableLiveData(interval)

    fun update( call: ActionInterval.() -> Unit){
        call.invoke(interval)
        ld.postValue(interval)
    }

    fun commit() = planVM.update {
        when(mode){
            Mode.CREATE -> {
                set(interval.from, interval.to, ApplianceAction.ON)
            }
            Mode.UPDATE -> {
                set(old.from, old.to, ApplianceAction.OFF)
                set(interval.from, interval.to, ApplianceAction.ON)
            }
        }

    }

    fun delete() = planVM.update {
        set(old.from, old.to, ApplianceAction.OFF)
    }

}

