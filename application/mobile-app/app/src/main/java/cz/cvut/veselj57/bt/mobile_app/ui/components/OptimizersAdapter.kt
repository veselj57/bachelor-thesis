package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.veselj57.bt.common.entities.BlockOptimizer
import cz.cvut.veselj57.bt.common.entities.Optimizer
import cz.cvut.veselj57.bt.common.entities.FixedOptimizer
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.FragmentOptimizationPlanDirections
import cz.cvut.veselj57.bt.mobile_app.ui.components.swipe_list.ListViewSwipeCallback
import kotlinx.android.synthetic.main.rv_optimizer_block.view.*
import java.lang.RuntimeException
import java.time.format.DateTimeFormatter

class OptimizersAdapter(val nav:  NavController ) : RecyclerView.Adapter<OptimizersAdapter.ViewHolder>(){



    sealed class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){

        val name = view.tv_name
        val from = view.tv_from
        val to = view.tv_to

        class BlockOptimizerVH(view: View): ViewHolder(view){
            val blocks = view.tv_blocks
            val lenght = view.tv_lenght
        }

        class FixedSchedule(view: View): ViewHolder(view)
    }

    private var items = listOf<Optimizer>()

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = when(items[position]){
        is BlockOptimizer -> 1
        is FixedOptimizer -> 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int)= when(type){
        1 -> ViewHolder.BlockOptimizerVH(inflate(parent, R.layout.rv_optimizer_block))
        2 -> ViewHolder.FixedSchedule(inflate(parent, R.layout.rv_optimizer_fixed))
        else -> throw  RuntimeException("View type $type not defined")
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val optimizer = items[position]

        holder.from.text = optimizer.from.format(DateTimeFormatter.ofPattern("HH:mm"))
        holder.to.text = optimizer.to.format(DateTimeFormatter.ofPattern("HH:mm"))
        holder.name.text = optimizer::class.simpleName

        holder.view.setOnClickListener {
            val action = when(optimizer){
                is BlockOptimizer -> FragmentOptimizationPlanDirections.editBlockOptimizer(position)
                is FixedOptimizer -> FragmentOptimizationPlanDirections.editFixedOptimizer(position)
            }
            nav.navigate(action)
        }

        when(holder){
            is ViewHolder.BlockOptimizerVH ->{
                val op = optimizer as BlockOptimizer

                holder.blocks.text = op.block_count.toString()
                holder.lenght.text = "${op.block_length*15}m"
            }

            is ViewHolder.FixedSchedule->{ }
        }

    }

    fun refreshWith(items: List<Optimizer>){
        this.items = items
        notifyDataSetChanged()
    }

    private fun inflate(parent: ViewGroup, layout: Int): View {
        return LayoutInflater.from(parent.context).inflate(layout, parent, false)
    }
}
