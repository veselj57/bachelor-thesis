package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.*

class DialogDatePicker : BaseDialog<DialogDatePicker.Callback>(),
    DatePickerDialog.OnDateSetListener {

    companion object{
        const val DATE = "date"

        fun showDialog(manager: FragmentManager, target: Fragment, date: LocalDate, request: Int = 0){
            val dialog = DialogDatePicker()
            dialog.setTargetFragment(target, request)
            dialog.arguments = bundleOf(
                    REQUEST_TAG to request,
                    DATE to date.format(ISO_DATE)
            )
            dialog.show(manager, "DialogDatePicker")
        }
    }

    interface Callback{
        fun onDateSet(time: LocalDate, request: Int): Boolean
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val d = LocalDate.parse(dArgs.getString(DATE), ISO_DATE)
        return  DatePickerDialog(dContext, this, d.year, d.monthValue, d.dayOfMonth)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if (callback.onDateSet(LocalDate.of(year, month, dayOfMonth), request))
            dismiss()

    }
}