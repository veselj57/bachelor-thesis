package cz.cvut.veselj57.bt.mobile_app.network

import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.mobile_app.conf.ServerConf
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.http.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * OpenStreetMapAPI mapper
 */
interface APIAppliance {

    /**
     * Get Address using GPS
     */
    @GET("/appliances")
    suspend fun  getAppliances(): List<Appliance>


    @GET("/appliances/{id}")
    suspend fun getAppliance(
        @Path("id") id: String,
        @Query("from") from: String?,
        @Query("to") to: String?
    ): Appliance


    @POST("/appliances/")
    suspend fun postAppliance(
        @Body appliance: Appliance
    ): Appliance


    @DELETE("/appliances/{id}")
    suspend fun deleteAppliance(
        @Path("id") id: String
    )


}
