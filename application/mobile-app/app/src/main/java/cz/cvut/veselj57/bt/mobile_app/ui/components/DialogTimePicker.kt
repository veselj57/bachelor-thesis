package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.*

class DialogTimePicker : BaseDialog<DialogTimePicker.Callback>(), TimePickerDialog.OnTimeSetListener {

    companion object{
        const val TIME = "hour"

        fun showDialog(manager: FragmentManager, target: Fragment, time: LocalTime, request: Int = 0){
            val dialog = DialogTimePicker()
            dialog.setTargetFragment(target, request)
            dialog.arguments = bundleOf(
                    REQUEST_TAG to request,
                    TIME to time.format(ISO_TIME)
            )
            dialog.show(manager, "DialogDatePicker")
        }
    }

    interface Callback{
        fun onTimeSet(time: LocalTime, request: Int): Boolean
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val t = LocalTime.parse(dArgs.getString(TIME), ISO_TIME)
        return  TimePickerDialog(dContext, this, t.hour, t.minute, true)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        if (callback.onTimeSet(LocalTime.of(hourOfDay, minute), request))
            dismiss()
    }
}