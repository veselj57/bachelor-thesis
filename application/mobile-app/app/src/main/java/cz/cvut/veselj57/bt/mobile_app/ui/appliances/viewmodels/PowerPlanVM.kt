package cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels

import androidx.lifecycle.*
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.mobile_app.scenario.repository.ElectricityPricesRepository
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.get
import java.time.LocalDate

import java.time.LocalTime

class PowerPlanVMVMFactory(val vm: ApplianceVM) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = PowerPlanVM(vm) as T
}

class PowerPlanVM(val vm: ApplianceVM) : ViewModel(), KoinComponent {

    val pricesRep = get<ElectricityPricesRepository>()

    lateinit var manager: PowerPlanManager
    val ldManager =  MutableLiveData<PowerPlanManager>()

    init {
        viewModelScope.launch {
            val plan = vm.appliance.partial_power_plan

            val from =  LocalTime.now().let {  LocalDate.now().atTime(it.hour, it.minute - it.minute%15) }
            // val from =  LocalDateTime.of(2020, 4, 12, 20, 15)

            val to =   plan.to

            val prices = pricesRep.getPrices(from, to)

            manager = PowerPlanManager(from, to, prices, plan.plan) //2020-04-12T20:15

            ldManager.postValue(manager)
        }

    }

    fun update( call: PowerPlanManager.() -> Unit){
        call.invoke(manager)
        ldManager.value = manager
    }

    fun commit() = vm.update {
        partial_power_plan.manager.setBy(manager)
        partial_power_plan.updateFromManager()
    }

}