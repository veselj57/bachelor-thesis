package cz.cvut.veselj57.bt.mobile_app.ui.prices

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.support.localized
import cz.cvut.veselj57.bt.mobile_app.ui.components.ElectricityPriceAdapter
import kotlinx.android.synthetic.main.fragment_prices.*
import java.time.LocalDate


class FragmentPrices : Fragment(), DatePickerDialog.OnDateSetListener {

    private val vm by viewModels<FragmentPricesVM>()
    private lateinit var adapter: ElectricityPriceAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_prices, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ElectricityPriceAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupChart(barChart)

        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        rv_prices.apply {
            adapter = this@FragmentPrices.adapter
            layoutManager = LinearLayoutManager(context!!)
            hasFixedSize()
        }

        vm.ldDate.observe(viewLifecycleOwner, Observer {
            tv_date1.text = it.localized()
            tv_date2.text = it.localized()
        })

        vm.ldPrices.observe(viewLifecycleOwner, Observer {
            adapter.refreshWith(it)
            barChart.invalidate()
            setData(barChart, it)
            barChart.isSelected = true
        })

        action_btn.setOnClickListener {
            val date = LocalDate.now()
            DatePickerDialog(context!!, this, date.year, date.monthValue, date.dayOfMonth).show()
        }

    }



    fun setupChart(chart: BarChart){
        chart.setDrawBarShadow(false)
        chart.setDrawValueAboveBar(false)
        chart.description.isEnabled = false
        chart.setDrawValueAboveBar(false)
        chart.setPinchZoom(false)
        chart.setDrawGridBackground(false)


        val xAxis: XAxis = chart.getXAxis()
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "${value.toInt()}:00"
            }

        }

        xAxis.labelCount = 8
        chart.legend.isEnabled = false
    }

    private fun setData(chart: BarChart, prices: List<ElectricityPrice>) {
        val values = prices.mapIndexed{index, price -> BarEntry(index.toFloat(), price.czk.toFloat()) }

        val set = BarDataSet(values, "")
        set.color = ResourcesCompat.getColor(resources, R.color.colorAccent, context!!.theme)
        set.setDrawIcons(false)
        set.setDrawValues(false)

        val data = BarData(listOf(set))
        data.setValueTextSize(10f)
        data.barWidth = 0.9f
        chart.data = data
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        vm.setDate(LocalDate.of(year, month, dayOfMonth))
    }
}



