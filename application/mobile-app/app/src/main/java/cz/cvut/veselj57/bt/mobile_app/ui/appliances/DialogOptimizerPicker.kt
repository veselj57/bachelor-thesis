package cz.cvut.veselj57.bt.mobile_app.ui.appliances

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import cz.cvut.veselj57.bt.mobile_app.R
import cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels.OptimizationPlanVM
import kotlinx.android.synthetic.main.dialog_optimizer_picker.view.*


class DialogOptimizerPicker: DialogFragment() {



    val vm by viewModels<OptimizationPlanVM>()

    val nav by lazy{ findNavController() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_optimizer_picker, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.view_add_optimizer.setOnClickListener {
            dismiss()
            nav.navigate(R.id.nav_action_new_block_optimizer)
        }

        view.view_fixed_optimizer.setOnClickListener {
            dismiss()
            nav.navigate(R.id.nav_action_new_fixed_optimizer)
        }
    }

}

