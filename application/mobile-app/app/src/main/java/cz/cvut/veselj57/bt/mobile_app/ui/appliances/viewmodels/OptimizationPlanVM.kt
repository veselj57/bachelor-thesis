package cz.cvut.veselj57.bt.mobile_app.ui.appliances.viewmodels

import androidx.lifecycle.*
import cz.cvut.veselj57.bt.common.entities.ActionInterval
import cz.cvut.veselj57.bt.common.entities.OptimizationPlan
import cz.cvut.veselj57.bt.mobile_app.ui.components.ActionIntervalAdapter

import java.time.LocalTime

class FragmentOptimizationPlanVMFactory(val position: Int, val vm: ApplianceVM) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = OptimizationPlanVM(
        position,
        vm
    ) as T
}

class OptimizationPlanVM(val position: Int, val vm: ApplianceVM) : ViewModel(){

    enum class Mode{ UPDATE, CREATE }

    val mode = if (position == -1) Mode.CREATE else Mode.UPDATE

    var plan: OptimizationPlan = when(mode){
        Mode.UPDATE -> vm.appliance.optimization_plans[position].copy()
        Mode.CREATE -> OptimizationPlan("", LocalTime.of(0, 0), LocalTime.of(0, 0), mutableListOf())
    }

    val ldPlan = MutableLiveData(plan)

    fun update( call: OptimizationPlan.() -> Unit){
        call.invoke(plan)
        ldPlan.value = plan
    }

    fun commit() = vm.update {
        when (mode){
            Mode.UPDATE -> optimization_plans[position] = plan
            Mode.CREATE -> optimization_plans.add(plan)
        }
    }

    fun delete() {
        if(mode == Mode.UPDATE){
            vm.update {
                this.deleteOptimizationPlan(position)
            }
        }
    }

}