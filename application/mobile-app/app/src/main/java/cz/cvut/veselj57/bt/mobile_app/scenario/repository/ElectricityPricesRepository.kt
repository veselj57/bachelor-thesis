package cz.cvut.veselj57.bt.mobile_app.scenario.repository

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.service.roundDownToHour
import cz.cvut.veselj57.bt.common.service.roundUpToHour
import cz.cvut.veselj57.bt.mobile_app.network.APIElectricityPrices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.time.LocalDateTime

class ElectricityPricesRepository(
    val API_prices: APIElectricityPrices
) :AbstractRepository() {

    val scope = CoroutineScope(Dispatchers.IO)

    val prices = sortedMapOf<LocalDateTime, ElectricityPrice>()

    suspend fun getPrices(
        _from: LocalDateTime, //inclusive
        _to: LocalDateTime   //inclusive
    ): List<ElectricityPrice> {
        val from = _from.roundDownToHour()
        val to = _to.roundUpToHour().plusHours(1L)

        if (!prices.containsKey(from) || !prices.containsKey(to)){
           API_prices.getPrices(from.toString(), to.toString()).forEach {
               prices[it.time] = it
           }
        }

        return prices.subMap(from, to).values.toList()
    }

}




