package cz.cvut.veselj57.bt.mobile_app.ui.components

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.*

class DialogDateTimePicker(
    val context: Context,
    val target: Callback,
    val datetime: LocalDateTime = LocalDateTime.now(),
    val request: Int = 1
): TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    interface Callback{
        fun onDateTimeSet(time: LocalDateTime, request: Int): Boolean
    }

    lateinit var date: LocalDate
    lateinit var time: LocalTime

    fun show(){
         DatePickerDialog(context, this, datetime.year, datetime.monthValue, datetime.dayOfMonth).show()
    }


    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        time = LocalTime.of(hourOfDay, minute)
        target.onDateTimeSet(date.atTime(time), request)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        date = LocalDate.of(year, month, dayOfMonth)
        TimePickerDialog(context, this, datetime.hour, datetime.minute, true).show()
    }

}