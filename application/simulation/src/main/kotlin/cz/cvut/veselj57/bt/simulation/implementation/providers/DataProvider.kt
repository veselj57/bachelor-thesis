package bt.veselj57.fel.cvut.cz.simulation.providers

import cz.cvut.veselj57.bt.common.service.PriceLookup
import java.time.LocalDate

interface DataProvider{
    val prices: PriceLookup
}