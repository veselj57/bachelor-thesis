
package cz.cvut.veselj57.bt.simulation.implementation.providers

import bt.veselj57.fel.cvut.cz.simulation.providers.DataProvider
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.service.PriceLookup
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDate
import java.time.LocalDateTime

class CSVDataProvider(file: String): DataProvider {

    override val prices: PriceLookup

    init {
        val reader: BufferedReader
        try {
            reader = BufferedReader(InputStreamReader(this::class.java.getResourceAsStream("/$file")))

            val dataset = sortedMapOf<LocalDateTime, ElectricityPrice>()


            var tokens  = (reader.readLine() ?: "").split(";")

            while (tokens.size == 3){
                val date = LocalDate.parse(tokens[0])
                val hour = tokens[1].toInt()-1
                val price = tokens[2].toDouble()/1000.0


                val datetime = date.atTime(hour, 0)
                dataset[date.atTime(hour, 0)] = ElectricityPrice(datetime, Double.NaN, price)

                tokens = (reader.readLine() ?: "").split(";")
            }

            prices = PriceLookup(dataset)


        }catch (e: Exception){
            throw RuntimeException(e)
            reader.close()
        }
    }


}
