package cz.cvut.veselj57.bt.simulation.simulations

import cz.cvut.veselj57.bt.common.service.PriceLookup
import simulation
import t


fun simulation_daily_market(year: Int, prices: PriceLookup) =



simulation(year, "$year - Summer Daily Market", prices) {
    appliance("heating", 10.0){
        winter(t(0, 0), t(0, 0)){
            block_optimizer(t(2, 0), t(7, 0), 4, 2)
            block_optimizer(t(14, 0), t(20, 0), 8, 2)
            block_optimizer(t(20, 0), t(0, 0), 4 , 2)
        }
    }

    appliance("water heating - heat pump ", 5.0){
        summer(t(0, 0), t(0, 0)){
            block_optimizer(t(0, 0), t(4, 0), 6, 1)
            block_optimizer(t(13, 0), t(19, 0), 6, 1)
            block_optimizer(t(19, 0), t(22, 0), 2 , 1)
        }
        winter(t(0, 0), t(0, 0)){
            block_optimizer(t(0, 0), t(4, 0), 3, 2)
            block_optimizer(t(13, 0), t(16, 0), 3, 2)
            block_optimizer(t(17, 0), t(19, 0), 4 , 1)
            block_optimizer(t(20, 0), t(0, 0), 4 , 2)
        }
    }

    appliance("Water Heating - boiler 1", 2.5){
        summer{
            block_optimizer(t(0), t(7 ), 6, 2)
            block_optimizer(t(13), t(17), 4, 2)
        }

        winter{
            block_optimizer(t(0), t(7), 4, 3)
            block_optimizer(t(13), t(17), 4, 2)
        }
    }

    appliance("Water Heating - boiler 2", 2.5){
        summer{
            block_optimizer(t(0), t(7), 6, 2)
            block_optimizer(t(13), t(17), 4, 2)
        }

        winter{
            block_optimizer(t(0), t(7), 4, 3)
            block_optimizer(t(13), t(17), 4, 2)
        }
    }

    appliance("AKU 1", 3.0){
        summer{}
        winter{
            block_optimizer(t(19), t(23), 4, 3)
        }
    }

    appliance("AKU 1", 3.0){
        summer{}
        winter{
            block_optimizer(t(19), t(23), 4, 3)
        }
    }

    appliance("Washing and drying machine", 2.0){
        summer{
            block_optimizer(t(18), t(22), 10, 1)
            block_optimizer(t(0), t(7), 10, 1)
        }
        winter{
            block_optimizer(t(18), t(22), 10, 1)
            block_optimizer(t(0), t(7), 10, 1)
        }
    }


    appliance("dishwasher", 2.0){
        summer{
            block_optimizer(t(19), t(7), 8, 1)
            block_optimizer(t(0), t(7), 8, 1)
        }
        winter{
            block_optimizer(t(19), t(7), 8, 1)
            block_optimizer(t(0), t(7), 8, 1)
        }
    }



    appliance("welness", 6.0){
        summer{

        }
        winter{
            scheduled(t(15, 30), t(18))
            scheduled(t(19), t(21))
        }
    }

    appliance("kitchen", 5.0){
        summer{
            scheduled(t(17), t(18, 30))
        }
        winter{
            scheduled(t(16), t(18, 30))
        }
    }


}

