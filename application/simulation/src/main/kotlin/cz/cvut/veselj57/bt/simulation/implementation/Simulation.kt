package bt.veselj57.fel.cvut.cz.simulation

import bt.veselj57.fel.cvut.cz.simulation.providers.DataProvider
import cz.cvut.veselj57.bt.common.entities.OptimizationPlan
import cz.cvut.veselj57.bt.common.extensions.toDec
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.common.service.PriceLookup
import java.lang.Appendable
import java.time.LocalDate
import java.time.LocalDateTime

import cz.cvut.veselj57.bt.common.extensions.until

//NT 1,184
//VT 1,815

class SimAppliance(val name: String, val consumption_kWh: Double, val seasons: MutableList<Season>){

    lateinit var manager: PowerPlanManager
    lateinit var starts: LocalDateTime
    lateinit var ends: LocalDateTime

    fun simulate(year: Int, price: PriceLookup){
        starts = LocalDateTime.of(year, 1, 1, 0, 0)
        ends = starts.plusYears(1)

        manager = PowerPlanManager(starts, ends, price.getPrices(starts, ends))

        for (season in seasons){
            season.optimization

            for (month in season.months){
                val from = LocalDate.of(year, month, 1)
                val to = from.plusMonths(1)

                for(date in from until to){
                    manager.applyOptimizationPlan(season.optimization, date)
                }
            }
        }

    }

    fun printStats(){
        println("Simulating: ${name.padEnd(30)}")
        println("kWhs: " + getEstimated_kWh().toDec(2) + ", cost:" + getEstimatedCost().toDec(2))
    }

    fun printSettings(){
        println("Simulating: ${name.padEnd(30)} consumption $consumption_kWh")
        for (season in seasons){
            println(season.name)
            println("--------------------")
            season.optimization.optimizers.forEach {
                print("$it ")
            }
            println()
            val daily_kWh = season.optimization.optimizers.sumByDouble { it.getActiveHours() } * consumption_kWh
            println("daily_kWh: $daily_kWh")
            println("--------------------")
        }
        println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")

    }

    fun getEstimatedCost() = manager.getEstimatedCost(consumption_kWh)
    fun getEstimated_kWh() = manager.getEstimated_kWh(consumption_kWh)
}

class Season(val optimization: OptimizationPlan, vararg val months: Int, val name: String = "")


class Simulation(
    val year: Int,
    val name: String,
    val prices: PriceLookup,
    val appliances: List<SimAppliance>

) {

    fun simulate(){
        appliances.forEach { it.simulate(year, prices) }
    }

    fun getEstimatedCost() = appliances.sumByDouble { it.getEstimatedCost() }
    fun getEstimated_kWh() = appliances.sumByDouble { it.getEstimated_kWh() }

}
