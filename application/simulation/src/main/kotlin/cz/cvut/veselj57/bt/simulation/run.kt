package cz.cvut.veselj57.bt.simulation

import bt.veselj57.fel.cvut.cz.simulation.Simulation
import bt.veselj57.fel.cvut.cz.simulation.providers.FixedTariffDataProvider
import cz.cvut.veselj57.bt.common.extensions.toDec
import cz.cvut.veselj57.bt.simulation.implementation.providers.CSVDataProvider
import cz.cvut.veselj57.bt.simulation.simulations.simulation_daily_market
import cz.cvut.veselj57.bt.simulation.simulations.simulation_fixed_tariff

fun main() {

    val x = CSVStats("datasets/2019.csv")
    print(x.average { it.datetime.hour <= 8 ||it.datetime.hour >= 22 }.toDec(2))


}


fun run(simulation: Simulation){
    simulation.simulate()
    println("${simulation.getEstimatedCost().toDec()} kc")
    //println("${simulation.getEstimated_kWh()} kWhs")

}

fun simulate() {
    val year = 2018

    // official prices

    //val vt = arrayOf(1.511, 1.511, 1.511, 1.828)
    //val nt = arrayOf(.895, .895, .895, 1.082)

    val vt = arrayOf(1.135, 1.135, 1.828)
    val nt = arrayOf(.730, .730, 1.082)

    //val vt = arrayOf(1.213, 1.213, 1.213)
    //val nt = arrayOf(1.213, 1.213, 1.213)


    val from = 2016
    val to = 2018
    for (year in from..to) {
        println("Year: $year")

        val dailyMarketPrices = CSVDataProvider("datasets/$year.csv").prices
        val fixedTariffPrices = FixedTariffDataProvider(year, vt[year - from], nt[year - from], arrayOf(16, 17, 22, 23, 0, 1, 2, 3)).prices


        print("Daily market: ")
        run(simulation_daily_market(year, dailyMarketPrices))
        print("Fixed tariff: ")
        run(simulation_fixed_tariff(year, fixedTariffPrices))
        println()
        println()

    }
}


