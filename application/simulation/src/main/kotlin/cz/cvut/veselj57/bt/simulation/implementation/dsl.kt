import bt.veselj57.fel.cvut.cz.simulation.Season
import bt.veselj57.fel.cvut.cz.simulation.SimAppliance
import bt.veselj57.fel.cvut.cz.simulation.Simulation
import bt.veselj57.fel.cvut.cz.simulation.providers.DataProvider


import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.BlockOptimizer as BlocOptimizerClass
import cz.cvut.veselj57.bt.common.service.PriceLookup
import org.litote.kmongo.newId
import java.time.LocalTime
import java.time.Year

class ApplianceBuilder(
    var name: String = "_",
    var consumption: Double = 0.0
){
    val seasons = mutableListOf<Season>()

    fun season(from: LocalTime, to: LocalTime, vararg months: Int, builder: SeasonBuilder.() -> Unit, name: String){
        val block =  SeasonBuilder(from, to, *months, name = name)
        builder.invoke(block)
        seasons.add(block.build())
    }

    fun winter(from: LocalTime =t(0, 0), to: LocalTime = t(0, 0), builder: SeasonBuilder.() -> Unit){
        season(from, to, 7, 8, 9, builder = builder, name = "winter")
    }

    fun summer(from: LocalTime =t(0, 0), to: LocalTime = t(0, 0), builder: SeasonBuilder.() -> Unit){
        season(from, to, 1, 2, 3, builder = builder, name = "summer")
    }

    fun build() = SimAppliance(name, consumption, seasons)

}

class SeasonBuilder(val from: LocalTime, val to: LocalTime, vararg val months: Int, val name: String){

    val optimizers = mutableListOf<Optimizer>()

    fun block_optimizer(
            from: LocalTime,
            to: LocalTime,
            blockLength: Int,
            numberOfBlocks: Int
    ){
        optimizers.add(BlocOptimizerClass(from, to, blockLength, numberOfBlocks))
    }

    fun scheduled(from: LocalTime, to: LocalTime){
        optimizers.add(FixedOptimizer(from, to))
    }

    fun build() = Season(OptimizationPlan("", from, to,optimizers), *months, name = name)
}

class SimulationBuilder {
    val appliances = mutableListOf<SimAppliance>()

    fun appliance(name: String, consumption: Double, x: ApplianceBuilder.() -> Unit) {
        val xxx = ApplianceBuilder(name, consumption)
        x.invoke(xxx)


        appliances.add(xxx.build())
    }

    fun build() = appliances
}



fun simulation(
    year: Int,
    name: String,
    data: PriceLookup,
    builder: SimulationBuilder.() -> Unit
): Simulation{
    val x = SimulationBuilder()
    builder.invoke(x)

    return Simulation(year,name, data, x.build()  )
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)

fun t(h: Int, m:Int = 0) = LocalTime.of(h, m)





