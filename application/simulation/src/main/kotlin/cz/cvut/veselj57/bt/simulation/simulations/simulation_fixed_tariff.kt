package cz.cvut.veselj57.bt.simulation.simulations

import cz.cvut.veselj57.bt.common.service.PriceLookup
import simulation
import t


fun simulation_fixed_tariff(year: Int, prices: PriceLookup) =



simulation(year, "$year - Summer Daily Market", prices) {
    appliance("heating", 10.0){
        winter{
          scheduled(t(16), t(19))
          scheduled(t(22), t(0))
          scheduled(t(1), t(4))
        }
    }

    appliance("water heating - heat pump ", 5.0){
        summer{
            scheduled(t(2, 30), t(4))
            scheduled(t(17), t(18, 30))
            scheduled(t(22), t(22, 30))
        }
        winter{
            scheduled(t(2), t(4))
            scheduled(t(14), t(16))
            scheduled(t(17), t(18))
            scheduled(t(22), t(23))

        }
    }

    appliance("Water Heating - boiler 1", 2.5){
        summer{
            scheduled(t(22), t(1))
            scheduled(t(17), t(19))
        }

        winter{
            scheduled(t(22), t(1))
            scheduled(t(17), t(19))
        }
    }

    appliance("Water Heating - boiler 2", 2.5){
        summer{
            scheduled(t(22), t(1))
            scheduled(t(17), t(19))
        }

        winter{
            scheduled(t(22), t(1))
            scheduled(t(17), t(19))
        }
    }


    appliance("AKU 1", 3.0){
        summer{}
        winter{
            scheduled(t(17), t(20))
        }
    }

    appliance("AKU 1", 3.0){
        summer{}
        winter{
            scheduled(t(17), t(20))
        }
    }

    appliance("Washing and drying machine", 2.0){
        summer{
            scheduled(t(17), t(19, 30))
            scheduled(t(22), t(0, 30))
        }
        winter{
            scheduled(t(17), t(19, 30))
            scheduled(t(22), t(0, 30))
        }
    }

    appliance("dishwasher 1", 2.0){
        summer{
            scheduled(t(22), t(0))
            scheduled(t(10), t(12))
        }
        winter{
            scheduled(t(22), t(0))
            scheduled(t(10), t(12))
        }
    }

    appliance("welness", 6.0){
        summer{

        }
        winter{
            scheduled(t(15, 30), t(18))
            scheduled(t(19), t(21))
        }
    }

    appliance("kitchen", 5.0){
        summer{
            scheduled(t(17), t(18, 30))
        }
        winter{
            scheduled(t(16), t(18, 30))
        }
    }

}

