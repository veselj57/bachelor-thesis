package cz.cvut.veselj57.bt.simulation

import bt.veselj57.fel.cvut.cz.simulation.providers.DataProvider
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.service.PriceLookup
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

data class Entry(val datetime: LocalDateTime, val price: Double)

class CSVStats(file: String) {

    val data = ArrayList<Entry>(365*24)


    init {
        val reader: BufferedReader
        try {
            reader = BufferedReader(InputStreamReader(this::class.java.getResourceAsStream("/$file")))

            var tokens  = (reader.readLine() ?: "").split(";")



            while (tokens.size == 3){
                println(tokens)
                val date = LocalDate.parse(tokens[0].trim())
                val hour = tokens[1].toInt()-1
                val price = tokens[2].toDouble()/1000

                data.add(Entry(date.atTime(hour, 0), price))

                tokens = (reader.readLine() ?: "").split(";")
            }
        }catch (e: Exception){
            throw RuntimeException(e)
            reader.close()
        }
    }

    fun average(predicate: (Entry)->Boolean): Double {
        var sum = 0.0
        var count = 0
        data.forEach {
            if(predicate.invoke(it)){
                sum += it.price
                count++
            }
        }
        return sum / count
    }


}
