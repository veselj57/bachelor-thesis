package bt.veselj57.fel.cvut.cz.simulation.providers


import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.extensions.until
import cz.cvut.veselj57.bt.common.service.PriceLookup
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class FixedTariffDataProvider(
        val year: Int,
        val high: Double,
        val low: Double,
        val lowHours: Array<Int>
): DataProvider {

    override val prices: PriceLookup

    init {
        val dayPrices = Array(24){high}
        lowHours.forEach { dayPrices[it] = low }


        val map  = TreeMap<LocalDateTime, ElectricityPrice>()

        val from = LocalDate.of(year, 1, 1)
        val to = LocalDate.of(year + 1, 1, 1)

        for (date in (from until to)){
            for (h in 0..23){
                val datetime = date.atTime(h, 0)
                map[datetime] = ElectricityPrice(datetime, dayPrices[h], dayPrices[h] )
            }
        }

        prices = PriceLookup(map)
    }

}
