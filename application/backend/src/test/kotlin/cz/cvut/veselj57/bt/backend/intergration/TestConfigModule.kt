package cz.cvut.veselj57.bt.backend.intergration

import cz.cvut.veselj57.bt.backend.config.DataSeeder
import cz.cvut.veselj57.bt.backend.config.JWTConfig
import cz.cvut.veselj57.bt.backend.config.KoinConfig
import cz.cvut.veselj57.bt.backend.persitence.exposed.ExposedDB
import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.backend.scheduled_tasks.CoreScheduledTask
import cz.cvut.veselj57.bt.backend.scheduled_tasks.PowerActionScheduledTask
import io.ktor.application.Application
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.get

object TestConfigModule: KoinComponent {


    /**
     * This module is used to install settings.
     * It is the first module to load.
     */
    @Suppress("unused")
    @kotlin.jvm.JvmOverloads
    fun Application.TestConfigModule(testing: Boolean = false) {

        JWTConfig.install(this)

        KoinConfig.configure()

        get<ExposedDB>().recreate()
        get<MongoDB>().recreate()
    }
}