package cz.cvut.veselj57.bt.backend.unit

import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.common.test.Seeder
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import kotlin.test.assertEquals

class PowerPlanManagerTest {

    @Test
    fun `set index`(){
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 1, 22, 0)
        val prices = Seeder.getHourPrices(from, to, Array(24){1.0})

        val manager = PowerPlanManager(from, to, prices)

        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        manager[2] = ApplianceAction.ON

        manager.actions.iterator().withIndex().forEach {
            if (it.index == 2)
                assertEquals(it.value, ApplianceAction.ON)
            else
                assertEquals(it.value, ApplianceAction.OFF)
        }

    }

    @Test
    fun `set range`(){
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 1, 22, 0)
        val prices = Seeder.getHourPrices(from, to, Array(24){1.0})

        val manager = PowerPlanManager(from, to, prices)

        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        val range = 2..4

        manager.set(range, ApplianceAction.ON)

        manager.actions.iterator().withIndex().forEach {
            if (range.contains(it.index))
                assertEquals(it.value, ApplianceAction.ON)
            else
                assertEquals(it.value, ApplianceAction.OFF)
        }
    }

    @Test
    fun `set by from to`(){
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 1, 22, 0)
        val prices = Seeder.getHourPrices(from, to, Array(24){1.0})

        val manager = PowerPlanManager(from, to, prices)

        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        val start = LocalDateTime.of(2020, 1, 1, 18, 45)
        val end = LocalDateTime.of(2020, 1, 1, 22, 0)

        manager.set(start, end, ApplianceAction.ON)

        manager.actions.iterator().withIndex().forEach {
            if (it.index < 27)
                assertEquals(it.value, ApplianceAction.OFF)
            else
                assertEquals(it.value, ApplianceAction.ON)
        }
    }


    @Test
    fun `end must be exclusive`(){
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 1, 22, 0)
        val prices = Seeder.getHourPrices(from, to, Array(24){1.0})

        val manager = PowerPlanManager(from, to, prices)

        assertEquals(manager.getTimeOf(manager.indices.last), LocalDateTime.of(2020, 1, 1, 21, 45))
    }

}