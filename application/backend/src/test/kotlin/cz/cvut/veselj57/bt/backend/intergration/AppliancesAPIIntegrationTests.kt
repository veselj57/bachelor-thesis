package cz.cvut.veselj57.bt.backend.intergration

import cz.cvut.veselj57.bt.backend.intergration.TestConfigModule.TestConfigModule
import cz.cvut.veselj57.bt.backend.modules.AppliancesAPI.ApplianceModule
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import io.ktor.application.Application
import io.ktor.http.*
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import kotlinx.serialization.builtins.list
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals


class AppliancesAPIIntegrationTests{

    val setup:  Application.() -> Unit  = {
        TestConfigModule()
        ApplianceModule()
    }

    @Test fun `create get`() = withTestApplication(setup) {
        val appliance = Appliance.getEmpty().apply { display_name = "Test name" }

        with(handleRequest(HttpMethod.Post, "/appliances/"){
            setBody(JsonContext().stringify(Appliance.serializer(), appliance))
        }){
            assertEquals(HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/appliances/")) {
            assertEquals(JsonContext().stringify(Appliance.serializer().list, listOf(appliance)), response.content)
        }
    }

    @Test
    fun `create delete`() = withTestApplication(setup) {
        val appl = Appliance.getEmpty().apply {
            display_name = "Test name"
        }

        with(handleRequest(HttpMethod.Post, "/appliances/"){
            setBody(JsonContext().stringify(Appliance.serializer(), appl))
        }){
            assertEquals(HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/appliances/")) {
            assertEquals(JsonContext().stringify(Appliance.serializer().list, listOf(appl)), response.content)
        }

        with(handleRequest(HttpMethod.Delete, "/appliances/${appl.id}/")) {
            assertEquals(HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/appliances/")) {
            assertEquals("[]", response.content)
        }
    }

    @Test
    fun `create update`() = withTestApplication(setup) {
        val appl = Appliance.getEmpty().apply {
            display_name = "Test name"
        }

        with(handleRequest(HttpMethod.Post, "/appliances/"){
            setBody(JsonContext().stringify(Appliance.serializer(), appl))
        }){
            assertEquals(HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/appliances/")) {
            assertEquals(JsonContext().stringify(Appliance.serializer().list, listOf(appl)), response.content)
        }

        with(handleRequest(HttpMethod.Post, "/appliances/") {
            setBody(JsonContext().stringify(Appliance.serializer(), appl))
        }) {
            assertEquals(HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/appliances/")) {
            assertEquals(JsonContext().stringify(Appliance.serializer().list, listOf(appl)), response.content)
        }
    }

}