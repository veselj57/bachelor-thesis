package cz.cvut.veselj57.bt.backend.unit

import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import cz.cvut.veselj57.bt.common.entities.BlockOptimizer
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.entities.OptimizationPlan
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import cz.cvut.veselj57.bt.common.test.Seeder
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import kotlin.test.assertEquals

class BlockOptimizerTest {

    @Test
    fun `best at start`(){
        val date = LocalDate.of(2020, 1, 1)
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 2, 12, 0)
        val _prices = Array(24){100.0}
        _prices[12] = 54.0
        _prices[13] = 54.0

        val prices = Seeder.getHourPrices(from, to, _prices)


        val manager = PowerPlanManager(from, to, prices)

        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }


        val optimizer = BlockOptimizer(from.toLocalTime(), to.toLocalTime(), 4, 2)
        val plan = OptimizationPlan("test", LocalTime.of(12, 0), LocalTime.of(12, 0), listOf(optimizer) )
        fun index(h: Int, m:Int) = manager.getIndexOf(plan.getDateTimeOf(date, LocalTime.of(h, m)))

        manager.applyOptimizationPlan(plan, LocalDate.of(2020, 1, 1))

        manager.actions.iterator().withIndex().forEach {
            if (it.index < index(14, 0))
                assertEquals(it.value, ApplianceAction.ON)
            else
                assertEquals(it.value, ApplianceAction.OFF)
        }

    }


    @Test
    fun `best at the end`(){
        val date = LocalDate.of(2020, 1, 1)

        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 2, 12, 0)
        val _prices = Array(24){100.0}
        _prices[10] = 54.0
        _prices[11] = 54.0
        val prices = Seeder.getHourPrices(from, to, _prices)

        val manager = PowerPlanManager(from, to, prices)


        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        val optimizer = BlockOptimizer(from.toLocalTime(), to.toLocalTime(), 4, 2)

        val plan = OptimizationPlan("test", LocalTime.of(12, 0), LocalTime.of(12, 0), listOf(optimizer) )

        fun index(h: Int, m:Int) = manager.getIndexOf(plan.getDateTimeOf(date, LocalTime.of(h, m)))


        manager.applyOptimizationPlan(plan, date)


        val split = index(10, 0)
        manager.actions.iterator().withIndex().forEach {
            if (it.index < split)
                assertEquals(it.value, ApplianceAction.OFF)
            else
                assertEquals(it.value, ApplianceAction.ON)
        }
    }

    @Test
    fun `best in the middle with a gap`(){
        val date = LocalDate.of(2020, 1, 1)
        val from = LocalDateTime.of(2020, 1, 1, 12, 0)
        val to = LocalDateTime.of(2020, 1, 2, 12, 0)

        val _prices = Array(24){100.0}
        _prices[16-1] = 54.0
        _prices[18-1] = 54.0
        val prices = Seeder.getHourPrices(from, to, _prices)

        val manager = PowerPlanManager(from, to, prices)

        manager.actions.forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        val optimizer = BlockOptimizer(LocalTime.of(15, 0), LocalTime.of(20, 0), 4, 2)

        val plan = OptimizationPlan("test", from.toLocalTime(), to.toLocalTime(), listOf(optimizer) )

        manager.applyOptimizationPlan(plan, LocalDate.of(2020, 1, 1))

        fun index(h: Int, m:Int) = manager.getIndexOf(date.atTime(h , m))

        manager.actions.sliceArray(index(12, 0)..index(14, 45)).forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        manager.actions.sliceArray(index(15, 0)..index(15, 45)).forEach {
            assertEquals(it, ApplianceAction.ON)
        }

        manager.actions.sliceArray(index(16, 0)..index(16, 45)).forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

        manager.actions.sliceArray(index(17, 0)..index(17, 45)).forEach {
            assertEquals(it, ApplianceAction.ON)
        }

        manager.actions.sliceArray(index(19, 0)..index(11, 45)).forEach {
            assertEquals(it, ApplianceAction.OFF)
        }

    }
}