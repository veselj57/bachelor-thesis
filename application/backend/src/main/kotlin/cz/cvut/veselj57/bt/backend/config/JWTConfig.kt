package cz.cvut.veselj57.bt.backend.config

import com.auth0.jwk.JwkProviderBuilder
import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import cz.cvut.veselj57.bt.backend.config.AuthenticatedUser.Companion.USER_ID_CLAIM

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.auth.Authentication
import io.ktor.auth.AuthenticationContext
import io.ktor.auth.Principal
import io.ktor.auth.jwt.jwt
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit

data class AuthenticatedUser(val id: Int) : Principal{

    companion object{
        const val USER_ID_CLAIM = "user_id"
    }
}

object JWTConfig {

    private const val secret = "zAP5MBA4B4Ijz0MZaS48"
    private const val issuer = "ktor.io"
    private const val validityInMs = 36_000_00 * 10 // 10 hours
    private val algorithm = Algorithm.HMAC512(secret)

    val jwkProvider = JwkProviderBuilder(issuer)
        .cached(10, 24, TimeUnit.HOURS)
        .rateLimited(10, 1, TimeUnit.MINUTES)
        .build()

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    /**
     * Produce a token for this combination of User and Account
     */
    fun generateToken(user: String): String = JWT.create()
        .withSubject("Authentication")
        .withIssuer(issuer)
        .withClaim(USER_ID_CLAIM, 1)
        //.withExpiresAt(getExpiration())
        .sign(algorithm)

    /**
     * Calculate the expiration Date based on current time + the given validity
     */
    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)

    /**
     * Install feature into application
     */
    fun install(app: Application) = app.install(Authentication) {
        jwt {
            verifier(verifier)
            validate {
                application.log.error("validate xx: ${it.payload.getClaim("id")}")

                //Should return valid user
                AuthenticatedUser(
                    it.payload.getClaim(
                        USER_ID_CLAIM
                    ).asInt()
                )
            }
        }
    }
}


fun AuthenticationContext.user() : AuthenticatedUser {
    val user = principal<AuthenticatedUser>()
    if(user == null){
        throw Exception("User not authenticated")
    }else{
        return user
    }
}


