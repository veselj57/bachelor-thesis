package cz.cvut.veselj57.bt.backend.persitence.mongo

import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking


class MongoDB(val config: Config) {


    val db = config.getMongoDatabase()

    val appliancesDAO = ApplianceDAO(db)



    fun recreate()  = runBlocking {
        appliancesDAO.clear()
    }
}



