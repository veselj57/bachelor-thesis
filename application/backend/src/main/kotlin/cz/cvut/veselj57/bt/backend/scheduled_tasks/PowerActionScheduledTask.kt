package cz.cvut.veselj57.bt.backend.scheduled_tasks

import cz.cvut.veselj57.bt.backend.core.PowerActionService
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import java.time.*
import java.util.*

object PowerActionScheduledTask: RepeatingTask(), KoinComponent{

    override val name: String = "PowerActionScheduledTask"

    override fun nextStart(): LocalDateTime {
        return LocalTime.now().run { LocalDate.now().atTime(hour, minute).plusMinutes(15L - minute % 15) }
    }

    override val period: Long = 15 * 60 * 1000

    override suspend fun fire() {
        PowerActionService().powerAppliancesByPowerPlan()
    }
}


