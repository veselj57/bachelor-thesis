package cz.cvut.veselj57.bt.backend.persitence.mongo

import com.mongodb.reactivestreams.client.MongoDatabase
import org.litote.kmongo.coroutine.CoroutineCollection


interface MongoCollectionDAO<T:Any> {

    val db: MongoDatabase
    val collection:  CoroutineCollection<T>

    abstract suspend fun drop();

    abstract suspend fun create();


    suspend fun clear(){
        drop()
        create()
    }

}