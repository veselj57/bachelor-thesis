package cz.cvut.veselj57.bt.backend.persitence.exposed

import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.entities.PowerPlanEntry
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import org.litote.kmongo.Id
import java.time.LocalDateTime

object PowerPlans : Table("power_plan") {
    val appliance_id = mongoId<Appliance>("_id")
    val time = datetime("time")
    val action = applianceAction("action")

    override val primaryKey = PrimaryKey(
        appliance_id,
        time
    )


    suspend fun getPowerPlan(id: Id<Appliance>, from: LocalDateTime, to: LocalDateTime): PowerPlan = transaction{
        val plan = transaction {
            PowerPlans.select{
                appliance_id.eq(id) and time.greaterEq(from) and  time.less(to)
            }.toList().map { it.toPowerPlanEntry() }
        }

        PowerPlan(from, to, plan)
    }

    suspend fun insert(id: Id<Appliance>, entry: PowerPlanEntry){
        PowerPlans.insert {
            it[appliance_id] = id
            it[action] = entry.action
            it[time] = entry.time
        }
    }

    fun insert(id: Id<Appliance>, entries: List<PowerPlanEntry>){
        PowerPlans.batchInsert(entries){
            this[PowerPlans.appliance_id] = id
            this[PowerPlans.time] = it.time
            this[PowerPlans.action] = it.action
        }
    }

    fun update(id: Id<Appliance>, plan: PowerPlan){
        transaction {
            PowerPlans.deleteWhere { time.greaterEq(plan.from) and time.less(plan.to) }
            insert(id, plan.plan)
        }
    }
}

fun ResultRow.toPowerPlanEntry() = PowerPlanEntry(this[PowerPlans.time], this[PowerPlans.action])

