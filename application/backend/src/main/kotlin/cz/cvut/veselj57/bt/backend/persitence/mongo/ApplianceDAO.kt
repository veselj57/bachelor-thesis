package cz.cvut.veselj57.bt.backend.persitence.mongo

import com.mongodb.reactivestreams.client.MongoDatabase
import cz.cvut.veselj57.bt.common.entities.*
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import org.litote.kmongo.*
import org.litote.kmongo.coroutine.*
import org.litote.kmongo.id.WrappedObjectId
import org.litote.kmongo.reactivestreams.getCollection
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter.ISO_INSTANT

class ApplianceDAO(override val db: MongoDatabase) :
    MongoCollectionDAO<Appliance> {

    override val collection = CoroutineCollection(db.getCollection<Appliance>())


    override suspend fun drop() {
        collection.drop()
    }

    override suspend fun create() {}

    suspend fun all(): List<Appliance> {
        return collection.find().toList()
    }

    suspend fun insert(appliance: Appliance) = collection.save(appliance)?.upsertedId.toString()

    suspend fun insertAll(appliances: List<Appliance>){
        appliances.forEach{ insert(it) }
    }

    suspend fun getPowerAction(datetime: LocalDateTime) =  collection.aggregate<PowerAction>("""[
        {
            ${"$"}project: {
                _id: 1,
                appliance_interface: 1,
                "partial_power_plan.plan": {
                    ${"$"}filter: {
                        input: "${"$"}partial_power_plan.plan",
                        as: "item",
                        cond: {
                            ${"$"}and: [
                                {${"$"}eq: ["${'$'}${"$"}item.time", ISODate("${datetime.atZone(ZoneId.systemDefault()).format(ISO_INSTANT)}")]},
                            ]
                        },
                    }
                }
            }
        },
        {
            ${"$"}project: {
                appliance_interface: 1,
                action: {${"$"}arrayElemAt: ["${"$"}partial_power_plan.plan", 0]}
            }
        },
        {
            ${"$"}project: {
                port: "${"$"}appliance_interface",
                action: "${"$"}action.action"
            }
        },
        {
            ${"$"}project: {
                port: 1,
                action:{ ${"$"}ifNull: [ "${"$"}action", "OFF" ]}
            }
        }
    ]""").toList()


    suspend  fun setRecentPowerPlan(id: Id<Appliance>, plan: PowerPlan){
        collection.updateOneById(id, setValue(Appliance::partial_power_plan,  plan))
    }

    suspend fun update(data:Appliance) {
        collection.updateOneById(data.id, data)
    }


}

fun <T> String.toId() = WrappedObjectId<T>(this)