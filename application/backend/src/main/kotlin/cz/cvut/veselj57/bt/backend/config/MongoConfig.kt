package cz.cvut.veselj57.bt.backend.config

import cz.cvut.veselj57.bt.backend.persitence.mongo.Config


object MongoConfig : Config {
    override val DATABASE_NAME = "bt"
    override val PORT = 27017
    override val IP = "localhost"
}
