package cz.cvut.veselj57.bt.backend.config

import cz.cvut.veselj57.bt.backend.persitence.exposed.Config


object ExposedConfig : Config {
    override val CONNECTION = "jdbc:postgresql://localhost:5432/bt"
    override val DRIVER = "org.postgresql.Driver"
    override val USER = "superuser"

    override val PASSWORD = "secret"
}
