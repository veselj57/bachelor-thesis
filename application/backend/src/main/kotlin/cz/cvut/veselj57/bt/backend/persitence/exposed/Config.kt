package cz.cvut.veselj57.bt.backend.persitence.exposed


interface Config{
    val CONNECTION: String
    val DRIVER: String
    val USER: String
    val PASSWORD: String
}


