package cz.cvut.veselj57.bt.backend.core

import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.common.entities.PowerAction
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class PowerActionService: KoinComponent {

    val mongo  by inject<MongoDB>()

    suspend fun powerAppliancesByPowerPlan(datetime: LocalDateTime = getNearestPowerTime()){
        val actions = mongo.appliancesDAO.getPowerAction(datetime)

        actions.forEach {
            powerAppliance(it)
        }
    }

    suspend fun powerAppliance(action: PowerAction){
        println("powering: $action")
    }

    fun getNearestPowerTime(): LocalDateTime {
        val date = LocalDate.now()
        val time = LocalTime.now()
        val mod = time.minute % 15

        val minutes = if (mod >= 8)   time.minute + (15 - mod) else  time.minute - mod

        return date.atTime(time.hour, 0).plusMinutes(minutes.toLong())
    }
}