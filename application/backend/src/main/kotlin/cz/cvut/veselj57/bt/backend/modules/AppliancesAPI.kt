package cz.cvut.veselj57.bt.backend.modules

import cz.cvut.veselj57.bt.backend.other.*
import cz.cvut.veselj57.bt.backend.core.PowerPlanService
import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import cz.cvut.veselj57.bt.backend.persitence.exposed.ExposedDB
import cz.cvut.veselj57.bt.backend.persitence.exposed.PowerPlans
import cz.cvut.veselj57.bt.backend.persitence.mongo.toId


import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.util.KtorExperimentalAPI
import kotlinx.serialization.builtins.list
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.LocalDateTime


/**
 * OfferModule contains all routes for CRUD offer
 */
object AppliancesAPI: KoinComponent {

    val mongo by inject<MongoDB>()
    val exposed by inject<ExposedDB>()
    val powerPlanPersistence by inject<PowerPlanService>()

    @KtorExperimentalAPI
    @Suppress("unused") // Referenced in application.conf
    @JvmOverloads
    fun Application.ApplianceModule(testing: Boolean = false) = routing {
        route("appliances") {
            get("", GET_appliances) // Option include items or not
            get("{id}", GET_appliance)
            get("{id}/power-plan/{from}/{to}", GET_power_plan)
            post("", POST_appliance)
            delete("{id}", DELETE_appliance)
        }
    }

    val GET_appliances: RouteHandler = {
        val (from, to) = badRequest {
            val param = call.parameters

            val from = param.getType("from"){ LocalDateTime.parse(this)}
            val to = param.getType("to"){ LocalDateTime.parse(this)}

            Pair(from, to)
        }

        val appliances = mongo.appliancesDAO.all()

        if (from != null && to != null){
            appliances.forEach {
                it.partial_power_plan = PowerPlans.getPowerPlan(it.id, from, to)
            }
        }

        call.respondJSON(Appliance.serializer().list, appliances)
    }

    val GET_appliance: RouteHandler = {
        val (id, from, to) = badRequest {
            val param = call.parameters

            val from = param.getType("from"){ LocalDateTime.parse(this)}
            val to = param.getType("to"){ LocalDateTime.parse(this)}
            val id = param["id"].toString().toId<Appliance>()

            Triple(id, from, to)
        }

        val appliance = badRequest {
            val appliance  = mongo.appliancesDAO.collection.findOneById(id) ?: return@badRequest  null

            if (from != null && to != null)
                appliance.partial_power_plan = PowerPlans.getPowerPlan(appliance.id, from, to)

            return@badRequest appliance
        }

        if (appliance == null)
            call.respond(HttpStatusCode.NoContent)
        else
            call.respondJSON(Appliance.serializer(), appliance )
    }


    val GET_power_plan: RouteHandler = {
        val (id, from, to) = badRequest {
            val param = call.parameters

            val from = LocalDateTime.parse(param["from"].toString())
            val to = LocalDateTime.parse(param["to"].toString())
            val id = param["id"].toString().toId<Appliance>()

            Triple(id, from, to)
        }

        val plan = badRequest {
            suspendedTransactionAsync {
                val appliance = mongo.appliancesDAO.collection.findOneById(id)
                if (appliance != null)
                    PowerPlans.getPowerPlan(id, from, to)
                else
                    null
            }.await()
        }

        if (plan != null)
            call.respondJSON(PowerPlan.serializer(), plan)
        else
            call.respond(HttpStatusCode.NoContent)
    }

    val POST_appliance: RouteHandler = {
        val appliance = badRequest {
            val appliance = JsonContext.default.parse(Appliance.serializer(), call.receiveText())

            mongo.appliancesDAO.collection.save(appliance)

            PowerPlans.update(appliance.id, appliance.partial_power_plan)
            appliance.partial_power_plan = powerPlanPersistence.getRecentPowerPlan(appliance.id)

            appliance
        }

        call.respondJSON(Appliance.serializer(), appliance )
    }


    val DELETE_appliance: RouteHandler = {
        val id = badRequest { call.parameters["id"]!!.toId<Appliance>() }
        mongo.appliancesDAO.collection.deleteOneById(id)
        call.respond(HttpStatusCode.OK)
    }


}





