package cz.cvut.veselj57.bt.backend.scheduled_tasks

import kotlinx.coroutines.runBlocking
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

abstract class RepeatingTask{

    companion object{
        val timer = Timer()
    }

    abstract val name: String

    val task = object : TimerTask(){

        init {
            this.scheduledExecutionTime()
        }
        override fun run() = runBlocking { fire() }
    }
    var runing = false
        private set

    abstract val period : Long

    abstract fun nextStart(): LocalDateTime

    abstract suspend fun fire()

    fun stop(){
        if (runing)
            task.cancel()
    }

    fun start(){

        if (!runing){
            val start = Date(nextStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())

            timer.scheduleAtFixedRate(task, start, period)
        }
    }

}