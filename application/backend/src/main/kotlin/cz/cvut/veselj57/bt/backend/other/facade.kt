package cz.cvut.veselj57.bt.backend.other

import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.common.entities.serializers.JsonContext
import io.ktor.application.ApplicationCall
import io.ktor.request.receiveText
import io.ktor.util.pipeline.PipelineContext
import kotlinx.serialization.CompositeDecoder
import kotlinx.serialization.CompositeEncoder
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json

typealias RouteHandler = suspend PipelineContext<Unit, ApplicationCall>.(Unit) -> Unit

inline fun <reified E : Enum<E>> random() = enumValues<E>().random()



suspend inline fun <T> ApplicationCall.respondJSON(
    serializer: KSerializer<List<T>>,
    entities: List<T>,
    json: Json = JsonContext.default
) = respondJSON(json.stringify(serializer,entities ))

suspend inline fun <T> ApplicationCall.respondJSON(
    serializer: KSerializer<T>,
    entities: T,
    json: Json = JsonContext.default
) = respondJSON(json.stringify(serializer,entities ))

suspend inline fun <T> ApplicationCall.receiveEntities(
    serializer: KSerializer<List<T>>,
    json: Json = JsonContext.default
) = json.parse(serializer, receiveText())

suspend inline fun <T> ApplicationCall.receiveEntity(
    serializer: KSerializer<T>,
    json: Json = JsonContext.default
) = json.parse(serializer, receiveText())
