package cz.cvut.veselj57.bt.backend.other

import io.ktor.application.ApplicationCall
import io.ktor.features.BadRequestException
import io.ktor.features.MissingRequestParameterException
import io.ktor.features.ParameterConversionException
import io.ktor.http.ContentType
import io.ktor.http.Parameters
import io.ktor.response.respondText
import io.ktor.util.KtorExperimentalAPI
import java.time.LocalDate

/*@KtorExperimentalAPI
inline fun <reified T> Parameters.getType(name: String, fc: String.() -> T): T {
    val param = this[name]
    if (param != null){
        try {
            return fc(param)
        } catch (e: Exception){
            throw ParameterConversionException(name, T::class.simpleName!!, e)
        }
    }else{
        throw MissingRequestParameterException(name)
    }
}*/

@KtorExperimentalAPI
inline fun <reified T> Parameters.getType(name: String, fc: String.() -> T): T? {
    val param = this[name]
    return if (param != null)
        try { fc(param) } catch (e: Exception){ null }
    else null
}

@KtorExperimentalAPI
inline fun <reified T> Parameters.getType(name: String, default : T?, fc: String.() -> T): T? {
    val param = this[name]
    if (param != null){
        try {
            return fc(param)
        } catch (e: Exception){
            throw ParameterConversionException(name, T::class.simpleName!!, e)
        }
    }else{
        return default
    }
}


/*

@KtorExperimentalAPI
fun Parameters.getLongOrThrowException(name: String) = getType(name, String::toLong)
*/


inline fun <T> badRequest(parse: ()-> T): T{
    try {
        return parse.invoke()
    }catch (e: Exception){
        throw BadRequestException("Exception in requestParse block.", e)
    }
}

inline fun Parameters.forEachWithCatch(
    default: Exception? = null,
    param: (String, List<String>) -> Unit
){
    try {
        this.entries().forEach{
            param.invoke(it.key, it.value)
        }

    }catch (e: Exception){
        throw default ?: BadRequestException("Exception in requestParse block.", default)
    }
}

suspend fun ApplicationCall.respondJSON(json: String) = respondText(
    contentType = ContentType.Application.Json,
    text = json
)


