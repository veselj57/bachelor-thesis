package cz.cvut.veselj57.bt.backend.core

import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices
import cz.cvut.veselj57.bt.backend.persitence.exposed.ExposedDB
import cz.cvut.veselj57.bt.common.entities.Appliance
import cz.cvut.veselj57.bt.backend.persitence.exposed.PowerPlans
import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import cz.cvut.veselj57.bt.common.entities.PowerPlanEntry
import cz.cvut.veselj57.bt.common.optimization.OptimizationService
import cz.cvut.veselj57.bt.common.service.PowerPlanManager
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.Id
import java.time.LocalDate

class PowerPlanService: KoinComponent {

    val mongo by inject<MongoDB>()
    val exposed by inject<ExposedDB>()
    val optimization by inject<OptimizationService>()
    val prices by inject<ElectricityPricesService>()

    suspend fun updateWith(id: Id<Appliance>, plan: PowerPlan){
        PowerPlans.update(id, plan)

        val recent = PowerPlans.getPowerPlan(id, PowerPlan.defaultFrom, PowerPlan.defaultTo)

        mongo.appliancesDAO.setRecentPowerPlan(id, recent)
    }

    suspend fun hardRefreshRecentPowerPlan(){
        val ids = mongo.appliancesDAO.all().map { it.id }

        ids.forEach {
            val plan = getRecentPowerPlan(it)
            mongo.appliancesDAO.setRecentPowerPlan(it, plan)
        }

    }

    suspend fun getRecentPowerPlan(id: Id<Appliance>) = PowerPlans.getPowerPlan(id, PowerPlan.defaultFrom, PowerPlan.defaultTo)


    suspend fun extendPowerPlan(date: LocalDate = LocalDate.now()){
        val appliances = mongo.appliancesDAO.all()

        for (appliance in appliances){
            val optimizationPlan = appliance.getCurrentOptimizationPlan() ?: continue

            val prices = ElectricityPrices.getPrices(optimizationPlan.getFrom(date), optimizationPlan.getTo(date))

            val plan = optimization.calcPowerPlan(optimizationPlan, date, prices)

            updateWith(appliance.id, plan)
        }

    }

}