package cz.cvut.veselj57.bt.backend.persitence.exposed

import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.litote.kmongo.Id
import org.litote.kmongo.toId


class ExposedDB(val config: Config) {

    init {
        Database.connect(config.CONNECTION, driver = config.DRIVER, user = config.USER, password = config.PASSWORD)
    }

    fun drop() = transaction {
        SchemaUtils.drop(PowerPlans, ElectricityPrices)
        PGEnum.drop<ApplianceAction>()
    }


    fun create() = transaction {
        PGEnum.create<ApplianceAction>()
        SchemaUtils.create(PowerPlans, ElectricityPrices)
    }

    fun recreate(){
        drop()
        create()
    }


}

fun <T> Table.mongoId(name: String): Column<Id<T>> = registerColumn(name, object : StringColumnType() {
    override fun sqlType() = "VARCHAR(24)"
    override fun valueFromDB(value: Any): Id<T> = (value as String).toId()
    override fun notNullValueToDB(value: Any): Any = value.toString()
})
