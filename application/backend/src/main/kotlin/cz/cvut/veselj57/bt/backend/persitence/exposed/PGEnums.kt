package cz.cvut.veselj57.bt.backend.persitence.exposed

import cz.cvut.veselj57.bt.common.entities.ApplianceAction
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import org.postgresql.util.PGobject

class PGEnum<T:Enum<T>>(enumTypeName: String, enumValue: T?) : PGobject() {

    companion object{
        inline fun <reified T:Enum<T>> create(name: String = T::class.simpleName!!) = transaction {
            val values = enumValues<T>().joinToString(", ") { "'$it'" }
            exec("CREATE TYPE $name AS ENUM ($values);")
        }

        inline fun <reified T:Enum<T>> drop(name: String = T::class.simpleName!!) = transaction {
            exec("DROP TYPE $name")
        }
    }

    init {
        value = enumValue?.name
        type = enumTypeName
    }
}


fun Table.applianceAction(column: String, name: String = ApplianceAction::class.simpleName!!) = customEnumeration(column, name,
    { value -> ApplianceAction.valueOf(value as String)},
    { PGEnum(name, it)}
)