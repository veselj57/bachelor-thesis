package cz.cvut.veselj57.bt.backend.core

import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices
import cz.cvut.veselj57.bt.common.source.ote.DailyMarketScraper
import org.koin.core.KoinComponent
import java.time.LocalDate

import cz.cvut.veselj57.bt.common.extensions.until
import cz.cvut.veselj57.bt.common.source.ote.CNBScraper
import java.time.LocalDateTime

class ElectricityPricesService(): KoinComponent {

    fun load(date: LocalDate): Boolean {
        return try {
            val rate = CNBScraper.scrape_EUR_CZK(date)

            val data =  DailyMarketScraper.scrapePrices(date, rate)

            ElectricityPrices.deleteRange(date.atStartOfDay(), date.atStartOfDay().plusDays(1))

            ElectricityPrices.batchInsert(data)

            true
        }catch (e: Exception){
            false
        }
    }


    fun load(from: LocalDate, to: LocalDate){
        (from until to).forEach { load(it) }
    }

}


