package cz.cvut.veselj57.bt.backend.config

import cz.cvut.veselj57.bt.backend.core.ElectricityPricesService
import cz.cvut.veselj57.bt.backend.core.PowerActionService
import cz.cvut.veselj57.bt.backend.core.PowerPlanService
import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices
import cz.cvut.veselj57.bt.backend.scheduled_tasks.PowerActionScheduledTask
import cz.cvut.veselj57.bt.common.test.Seeder
import cz.cvut.veselj57.bt.backend.persitence.exposed.ExposedDB
import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.common.optimization.OptimizationService
import org.koin.core.context.startKoin
import org.koin.dsl.module

object KoinConfig {
    val module = module {
        single { MongoDB(MongoConfig) }
        single { ExposedDB(ExposedConfig) }
        single { Seeder }

        single { PowerPlanService() }
        single { PowerActionService() }
        single { ElectricityPricesService() }
        single { OptimizationService() }
    }

    fun configure() = startKoin {
        modules(module)
    }

}