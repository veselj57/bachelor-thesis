package cz.cvut.veselj57.bt.backend.persitence.exposed

import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices.price_czk
import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices.price_eur
import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices.utc_datetime
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import java.time.ZoneId

object ElectricityPrices : Table("electricity_prices") {
    val utc_datetime = datetime("time")
    val price_czk = double("czk")
    val price_eur = double("eur")

    override val primaryKey = PrimaryKey(utc_datetime)

    suspend fun getPricesByUTC(from: LocalDateTime, to: LocalDateTime): List<ElectricityPrice> = transaction {
        ElectricityPrices
            .select { utc_datetime.greaterEq(from) and utc_datetime.less(to) }
            .orderBy(utc_datetime)
            .map { it.toElectricityPrice() }
    }

    suspend fun getPrices(from: LocalDateTime, to: LocalDateTime): List<ElectricityPrice> {
        val zone = ZoneId.systemDefault()
        from.atZone(zone).toLocalDateTime()

        return getPricesByUTC(
            from.atZone(zone).toLocalDateTime(),
            to.atZone(zone).toLocalDateTime()
        ).map { it.apply { time = time.atZone(zone).toLocalDateTime() } }
    }


    fun batchInsert(values: Iterable<ElectricityPrice>) = batchInsert(values) {
        this[utc_datetime] = it.time.atZone(ZoneId.systemDefault()).toLocalDateTime()
        this[price_czk] = it.czk
        this[price_eur] = it.czk
    }

    fun deleteRange(from: LocalDateTime, to: LocalDateTime) = transaction{
        ElectricityPrices.deleteWhere { utc_datetime.greaterEq(from) and utc_datetime.less(to) }
    }
}

fun ResultRow.toElectricityPrice() = ElectricityPrice(this[utc_datetime], this[price_czk].toDouble(), this[price_eur].toDouble())
