package cz.cvut.veselj57.bt.backend.scheduled_tasks

import cz.cvut.veselj57.bt.backend.core.ElectricityPricesService
import cz.cvut.veselj57.bt.backend.core.PowerPlanService
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.*

object CoreScheduledTask: RepeatingTask(), KoinComponent {

    private val pricesService by inject<ElectricityPricesService>()
    private val powerPlanService by inject<PowerPlanService>()

    override val name: String = "CoreScheduledTask"

    override val period: Long = 24 * 60 * 60 *1000

    override fun nextStart(): LocalDateTime {
        val date = LocalDate.now().atTime(14, 0)

        return if (date < LocalDateTime.now()) date.plusDays(1L) else date
    }

    override suspend fun fire() {
        val date = LocalDate.now()

        pricesService.load(date.plusDays(1))

        powerPlanService.extendPowerPlan(date)
    }


}


