package cz.cvut.veselj57.bt.backend.config

import cz.cvut.veselj57.bt.backend.core.PowerPlanService
import cz.cvut.veselj57.bt.common.test.Seeder
import cz.cvut.veselj57.bt.backend.persitence.exposed.*
import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.common.entities.PowerPlan
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.KoinComponent
import org.koin.core.get


class DataSeeder: KoinComponent {
    val seeder = get<Seeder>()

    val mongo = get<MongoDB>()
    val exposed = get<ExposedDB>()
    val powerPlanManager = get<PowerPlanService>()

    suspend fun seed(){
        mongo.appliancesDAO.clear()

        transaction {
            exposed.drop()
            exposed.create()
        }

        val from = PowerPlan.defaultFrom
        val to = PowerPlan.defaultTo

        val prices = Seeder.getHourPrices(from, to)

        transaction {
            ElectricityPrices.batchInsert(prices)
        }


        val appliances = Seeder.getAppliances(from, to, 7, prices)
        mongo.appliancesDAO.insertAll(appliances)

        val ids = appliances.map {it.id}

        suspendedTransactionAsync {
            ids.forEach {
                PowerPlans.insert(it, seeder.getPowerPlanManager(from, to).getPowerPlanEntries())
            }

            powerPlanManager.hardRefreshRecentPowerPlan()
        }.await()
    }
}