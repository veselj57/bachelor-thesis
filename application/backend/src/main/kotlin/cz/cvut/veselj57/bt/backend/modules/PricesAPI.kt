package cz.cvut.veselj57.bt.backend.modules

import cz.cvut.veselj57.bt.backend.persitence.mongo.MongoDB
import cz.cvut.veselj57.bt.backend.other.RouteHandler
import cz.cvut.veselj57.bt.backend.other.badRequest
import cz.cvut.veselj57.bt.backend.other.respondJSON
import cz.cvut.veselj57.bt.common.entities.ElectricityPrice
import cz.cvut.veselj57.bt.backend.persitence.exposed.ElectricityPrices


import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.routing.*
import io.ktor.util.KtorExperimentalAPI
import kotlinx.serialization.builtins.list
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.time.LocalDateTime


/**
 * OfferModule contains all routes for CRUD offer
 */
object PricesAPI: KoinComponent {

    val mongo by inject<MongoDB>()

    @KtorExperimentalAPI
    @Suppress("unused") // Referenced in application.conf
    @JvmOverloads
    fun Application.PricesModule(testing: Boolean = false){
        routing {
            route("electricity-prices") {
                get("/", GET_all_matching)
            }
        }
    }

    val GET_all_matching: RouteHandler = {
        val (from, to) = badRequest {
            Pair(
                LocalDateTime.parse(call.parameters["from"].toString()),
                LocalDateTime.parse(call.parameters["to"].toString())
            )
        }

        call.respondJSON(
            ElectricityPrice.serializer().list,
            ElectricityPrices.getPrices(from, to)
        )
    }

}





