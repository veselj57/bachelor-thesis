package cz.cvut.veselj57.bt.backend.modules

import cz.cvut.veselj57.bt.backend.config.DataSeeder
import cz.cvut.veselj57.bt.backend.config.JWTConfig
import cz.cvut.veselj57.bt.backend.config.KoinConfig
import cz.cvut.veselj57.bt.backend.scheduled_tasks.CoreScheduledTask
import cz.cvut.veselj57.bt.backend.scheduled_tasks.PowerActionScheduledTask
import io.ktor.application.Application
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import java.util.logging.Level
import java.util.logging.Logger

object ModuleConfig: KoinComponent{

    /**
     * This module is used to install settings.
     * It is the first module to load.
     */
    @Suppress("unused")
    @kotlin.jvm.JvmOverloads
    fun Application.ConfigModule(testing: Boolean = false) {

        JWTConfig.install(this)

        KoinConfig.configure()

        runBlocking {
            DataSeeder().seed()
        }

        PowerActionScheduledTask.start()
        CoreScheduledTask.start()
    }

}