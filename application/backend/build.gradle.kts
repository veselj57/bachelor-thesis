
val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project

plugins {
    application
    java
    id("com.github.johnrengelman.shadow") version "5.2.0"
    kotlin("jvm") version "1.3.70"
}

group = "cz.cvut.veselj57.bt"
version = "1.0"

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = application.mainClassName
    }

    from(configurations.runtime.get().map {if (it.isDirectory) it else zipTree(it)})
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
    maven { url = uri("https://plugins.gradle.org/m2/") }
}

dependencies {
    implementation("cz.cvut.veselj57.bt:common:1.0")


    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")



    val koin_version = "2.1.5"
    implementation ("org.koin:koin-core:$koin_version")
    implementation ("org.koin:koin-core-ext:$koin_version")
    testImplementation ("org.koin:koin-test:$koin_version")
    implementation ("org.koin:koin-ktor:$koin_version")



    implementation("org.jetbrains.exposed", "exposed-core", "0.22.1")
    implementation("org.jetbrains.exposed", "exposed-dao", "0.22.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.22.1")
    implementation("org.jetbrains.exposed", "exposed-java-time", "0.22.1")
    implementation( "org.postgresql:postgresql:42.1.4")


    implementation("org.litote.kmongo:kmongo-coroutine-serialization:4.0.0")
    implementation("org.litote.kmongo:kmongo-serialization-mapping:4.0.0")
    implementation("org.litote.kmongo:kmongo-id:4.0.0")


    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

