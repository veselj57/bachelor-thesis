package bt.veselj57.fel.cvut.cz.strategies

import java.time.LocalTime

/**
 * od 15:00 do 20:00
 * top dvě hodiny
 * minimální doba dopení je půl hodina
 */
data class BlockOptimizer(
    override val from: LocalTime,
    override val to: LocalTime,
    val blockLength: Int,
    val numberOfBlocks: Int,
    var weights:Array<Double> = Array(4*24){1.0}
) : PowerStrategy() {

    override fun calc(plan: PowerPlan) {
        val interval = plan.getIndeces(from, to)

        val block = Array(numberOfBlocks){
                i ->interval.first + i * blockLength
        }
        val termination = Array(numberOfBlocks){
                i -> interval.last - (numberOfBlocks -i) * blockLength+1
        }

        var bestCombination = Array(numberOfBlocks){-1}
        var bestSum = Double.POSITIVE_INFINITY


        loop@while (true){
            var sum = 0.0
            for (i in block.indices){
                for (c in block[i] until block[i]+blockLength){
                    sum += plan.getPrice(c)* weights[c-interval.first]
                }
            }

            if (sum < bestSum){
                bestSum = sum
                bestCombination = block.clone()
            }

            for ( i in block.indices){
                // Find which block is on termination stage
                if (block[i] == termination[i]){

                    // If first block is on termination stage
                    // all other blocks are on termination stage as well
                    if (i == 0) break@loop

                    // Move the block before
                    block[i-1]++

                    // Reset position of blocks
                    // after the block that was moved
                    for(x in i..block.indices.last)
                        block[x] =block[x-1] + blockLength

                    continue@loop
                }
            }
            block[block.size-1]++
        }

        //Update power plan
        for (b in bestCombination.indices){
            for (i in bestCombination[b] until bestCombination[b]+blockLength){
                plan[i] = ApplianceAction.ON
            }
        }
    }
}


