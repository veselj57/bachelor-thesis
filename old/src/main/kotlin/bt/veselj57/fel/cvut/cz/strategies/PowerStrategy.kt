package bt.veselj57.fel.cvut.cz.strategies

import bt.veselj57.fel.cvut.cz.HourPrice
import java.time.LocalDateTime
import java.time.LocalTime

fun iterator(from: LocalDateTime, to: LocalDateTime, minutes: Long) = object: Iterator<LocalDateTime>{
    var iter = from
    override fun hasNext() = iter.isBefore(to)
    override fun next() = iter.also { iter = it.plusMinutes(minutes)  }
}


enum class ApplianceAction{ON, OFF}

abstract class PowerStrategy{
    abstract val from: LocalTime
    abstract val to: LocalTime
    abstract fun calc(plan: PowerPlan)
}