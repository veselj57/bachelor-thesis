package bt.veselj57.fel.cvut.cz.strategies

import java.time.LocalTime



data class CheapestNSeries(
    override val from: LocalTime,
    override val to: LocalTime,
    val sequenceSize: Int,
    val weights:Array<Double>
) : PowerStrategy() {

    override fun calc(plan: PowerPlan){
        val interval = plan.getIndeces(from, to)


        var bestPrices = Double.POSITIVE_INFINITY
        var bestStart = interval.first
        for ( c in interval.first..interval.last-sequenceSize+1){
            var temp = 0.0
            for (s in c until  sequenceSize + c){
                temp += plan.getPrice(s) * weights[s-interval.first]
            }

            if (temp < bestPrices){
                bestPrices = temp
                bestStart = c
            }
        }

        for ( i in bestStart until bestStart+sequenceSize){
            plan[i] = ApplianceAction.ON
        }

    }
}
