
package bt.veselj57.fel.cvut.cz.simulation

import bt.veselj57.fel.cvut.cz.simulation.providers.CSVDataProvider
import bt.veselj57.fel.cvut.cz.simulation.providers.FixedTariffDataProvider
import simulation
import t
import java.time.LocalDate

fun main() {

    val year = 2015
    val vt = arrayOf(1.511, 1.511, 1.511, 1.828)
    val nt = arrayOf(.895, .895, .895, 1.082)
    //val vt = arrayOf(1.143, 1.143, 1.143, 1.143)
    //val nt = arrayOf(.753, .753, .753, 0.753)

    for (i in 0 until 4){
        val year = year + i
        val from = LocalDate.of(year, 1, 1)
        val to = LocalDate.of(year, 12, 31)

        val real_data = CSVDataProvider("datasets/$year.csv").getDataset(from, to, 0)

        simulation("$year - Real data", real_data, 30) {
            season(1, 2, 3, 4){
                appliance("Heating", 10.0){
                    strategy(t(2, 0), t(6, 30), 2, 2)
                    strategy(t(14, 0), t(19, 30), 4, 2)
                    strategy(t(20, 0), t(23, 30), 4 , 1)
                }
                appliance("Hot Water", 5.0){
                    strategy(t(0, 0), t(3, 30), 3, 1)
                    strategy(t(13, 0), t(15, 30), 3, 1)
                    strategy(t(17, 0), t(18, 30), 2 , 1)
                    strategy(t(20, 0), t(23, 30), 4 , 1)
                }
            }
            season(5, 6, 7, 8, 9, 10, 11, 12){
                appliance("Hot Water", 5.0){
                    strategy(t(0, 0), t(3, 30), 3, 1)
                    strategy(t(13, 0), t(15, 30), 3, 1)
                    strategy(t(17, 0), t(18, 30), 2 , 1)
                    strategy(t(20, 0), t(23, 30), 4 , 1)
                }
            }
        }.simulate().results()


        val fixed_data = FixedTariffDataProvider(vt[i], nt[i], arrayOf(16, 17, 22, 23, 0, 1, 2, 3))
            .getDataset(from, to, 0)

        simulation("$year - Fixed tariff", fixed_data, 30) {
            season(1, 2, 3, 4){
                appliance("Heating", 10.0){
                    preset(t(7, 0), t(7, 30))
                    preset(t(16, 0), t(18, 30))
                    preset(t(22, 0), t(23, 30))
                    preset(t(0, 0), t(1, 30))
                }
                appliance("Hot Water", 5.0){
                    preset(t(17, 0), t(18, 30))
                    preset(t(22, 0), t(23, 30))
                    preset(t(0, 0), t(1, 30))
                }
            }
            season(5, 6, 7, 8, 9, 10, 11, 12){
                appliance("Hot Water", 5.0){
                    preset(t(17, 0), t(18, 30))
                    preset(t(22, 0), t(23, 30))
                    preset(t(0, 0), t(1, 30))
                }
            }
        }.simulate().results()

        println()

    }

}




