package bt.veselj57.fel.cvut.cz.simulation

import bt.veselj57.fel.cvut.cz.simulation.appliances.Appliance
import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import format

//NT 1,184
//VT 1,815

class Season(val months: List<Int>, val appliances: List<Appliance>){

    val prices = mutableListOf<IntervalPrices>()

    fun simulate(step: Int){
        prices.forEach {
            i -> appliances.forEach { it.process(i, step) }
        }
    }
}

class Simulation(
    val name: String,
    val data: List<IntervalPrices>,
    val step: Int,
    val seasons: List<Season>

) {
    init {
        data.forEach {
            x -> seasons.first { it.months.contains(x.from.month.value)}.prices.add(x)
        }
    }

    fun simulate(): Simulation{
        seasons.forEach { x -> x.simulate(step) }
        return this
    }

    fun results() {


        val energy = seasons.map { it.appliances.map { it.getTotalEnergyConsumption() }.sum() }.sum()
        val bill = seasons.map { it.appliances.map { it.getTotalBill() }.sum() }.sum()

       // println("$name - energy consumed:  ${energy.format(2)} ")
        println("$name - total cost:  ${bill.format(2)} ")
    }

}
