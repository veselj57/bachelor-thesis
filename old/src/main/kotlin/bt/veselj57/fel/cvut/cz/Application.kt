package bt.veselj57.fel.cvut.cz

import io.ktor.application.ApplicationCall
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

typealias RouteHandler = suspend PipelineContext<Unit, ApplicationCall>.(Unit) -> Unit

inline fun <reified E : Enum<E>> random() = enumValues<E>().random()



