
package bt.veselj57.fel.cvut.cz.simulation.providers

import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDate


class CSVDataProvider(val file: String):DataProvider {
    private val reader: BufferedReader

    val dataset = sortedMapOf<LocalDate, Array<Double>>()

    init {
        try {
            reader = BufferedReader(InputStreamReader(this::class.java.getResourceAsStream("/$file")))

            var tokens  = (reader.readLine() ?: "").split(";")

            val day = Array(24){ Double.NaN}
            var xx: LocalDate = LocalDate.parse(tokens[0])

            while (tokens.size == 3){

                val date = LocalDate.parse(tokens[0])
                val hour = tokens[1].toInt()-1
                val price = tokens[2].toDouble()/1000.0

                if (xx != date){
                    dataset[xx] = day.copyOf()
                    xx = date
                    day.fill(999.99)
                }

                day[hour] = price

                tokens = (reader.readLine() ?: "").split(";")
            }
        }catch (e: Exception){
            throw RuntimeException(e)
        }finally {

        }
    }

    override fun getDataset(from: LocalDate, to: LocalDate, offset: Int): List<IntervalPrices> {
        val range = dataset.subMap(from, to).entries.toTypedArray()

        val prices =  Array(range.size * 24){Double.NaN}

        for ( i in range.indices){
            range[i].value.copyInto(prices, i*24)

        }

        val list = ArrayList<IntervalPrices>(range.size)

        for ( i in 0..range.size-2){
            val day = range[i].key
            list.add(IntervalPrices(
                day.atTime(offset, 0),
                day.atTime(offset, 0).plusHours(24L),
               prices.copyOfRange(offset + 24 * i, offset + 24*(i+1))
            ))
        }

        //println("dataset size: " + list.size)

        return list
    }



}
