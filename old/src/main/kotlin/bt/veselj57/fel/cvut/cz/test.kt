package bt.veselj57.fel.cvut.cz

internal object GFG {
    fun nCr(n: Int, r: Int): Int {
        val fac = IntArray(10000)
        for (i in 0 until n) fac[i] = 1
        for (i in 1 until n + 1) {
            fac[i] = fac[i - 1] * i
        }
        return fac[n] / (fac[n - r] *
                fac[r])
    }

    // Driver Code
    @JvmStatic
    fun main(args: Array<String>) {
        val n = 96
        val k = 48
        val ans = nCr(n + k - 1, k) +
                nCr(k - 1, n - 1)
        println(ans)
    }
}

// This code is contributed
// by anuj_67