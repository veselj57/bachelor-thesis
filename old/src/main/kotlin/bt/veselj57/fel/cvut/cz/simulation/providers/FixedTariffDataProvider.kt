package bt.veselj57.fel.cvut.cz.simulation.providers

import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import java.time.LocalDate
import java.time.LocalDateTime

class FixedTariffDataProvider(
    val high: Double,
    val low: Double,
    val low_hours: Array<Int>
): DataProvider {

    override fun getDataset(from: LocalDate, to: LocalDate, offset: Int): List<IntervalPrices> {
        val dataset = mutableListOf<IntervalPrices>()

        val prices = Array(24){high}.apply {
            low_hours.forEach { this[it] = low }
        }

        var iter = from

        while (iter.isBefore(to.minusDays(1L))){
            dataset.add( IntervalPrices(
                iter.atTime(offset, 0),
                iter.atTime(offset, 0).plusHours(24L),
                prices
            ))

            iter = iter.plusDays(1L)
        }

        //println("dataset size: " + dataset.size)

        return dataset
    }

}