package bt.veselj57.fel.cvut.cz.strategies

import bt.veselj57.fel.cvut.cz.HourPrice
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime

class DailyPriceDataset(
    val list: List<HourPrice>
){
    init {
        list.sortedBy { it.price }
    }

    val from = list.first().time
    val to = list.last().time

    //operator fun get(i:Int) = list[i].price
    operator fun get(time: LocalTime) = list.first { time.withMinute(0) == it.time.toLocalTime() }.price


    fun getPrices(from: LocalTime, to: LocalTime) = list.subList(getPositionOf(from), getPositionOf(to))

    fun getPositionOf(time: LocalTime):Int{
        val x = Duration.between(from.toLocalTime(), time).toMinutes().toInt()/60
        return if (x < 0)  24+x else x
    }
}