package bt.veselj57.fel.cvut.cz.strategies

import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime


data class IntervalPrices(
    val from: LocalDateTime,
    val to: LocalDateTime,
    val prices: Array<Double>
){
    init {
        check(prices.size == 24){"Shit at $from, $to"}
    }
    override fun toString() = "($from, $to): ${prices.toString()}"
}

class PowerPlan(
    val dataset: IntervalPrices,
    val step:Int
){
    val executions = 24*60/step
    val indeces = IntRange(0, executions-1)

    val actions = Array(executions) {ApplianceAction.OFF}

    fun getPrice(i: Int):Double = dataset.prices[(i*step/60)]


    fun getPositionOf(time: LocalTime):Int{
        val x = getNumberOfExecutions(dataset.from.toLocalTime(), time)
        return if (x < 0)  executions+x else x
    }

    data class Entry(val position:Int, val time:LocalDateTime, val action: ApplianceAction){
        override fun toString() = "$position \t $time  $action"
    }


    fun getAllEntries() = Array(executions){ i-> this[i]}

    fun getIndeces(from: LocalTime, to: LocalTime) = IntRange(getPositionOf(from), getPositionOf(to))

    fun getNumberOfExecutions(from: LocalTime, to: LocalTime) = Duration.between(from, to).toMinutes().toInt()/step

    operator fun get(p: Int) = Entry(p, dataset.from.plusMinutes((p*step).toLong()), actions[p])


    operator fun set(p: Int, action: ApplianceAction){ actions[p] = action}

    override fun toString() = indeces.fold(""){acc, i -> acc + this[i] }

    fun toString(from: LocalTime, to: LocalTime, dataset: DailyPriceDataset) = getIndeces(from, to).fold(""){
            acc, i ->
        val item =  this[i]
        acc + item + " " + dataset[item.time.toLocalTime()] + "\n"
    }


}
