import bt.veselj57.fel.cvut.cz.simulation.Season
import bt.veselj57.fel.cvut.cz.simulation.Simulation


import bt.veselj57.fel.cvut.cz.simulation.appliances.Appliance
import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import bt.veselj57.fel.cvut.cz.strategies.BlockOptimizer
import bt.veselj57.fel.cvut.cz.strategies.PowerStrategy
import bt.veselj57.fel.cvut.cz.strategies.SimpleStrategy
import java.time.LocalTime

class ApplianceBuilder( var name: String = "_",
                        var consumption: Double = 0.0){
    val strategies = mutableListOf<PowerStrategy>()

    fun strategy(
        from: LocalTime,
        to: LocalTime,
        blockLength: Int,
        numberOfBlocks: Int,
        weights:Array<Double> = Array(4*24){1.0}
    ){
        strategies.add(BlockOptimizer(from, to, blockLength, numberOfBlocks, weights))
    }

    fun preset(from: LocalTime, to: LocalTime){
        strategies.add(SimpleStrategy(from, to))
    }

    fun build() = Appliance(name, consumption, strategies)
}

class BlockBuilder(vararg months: Int){
    val months = months.toList()


    val appliances = mutableListOf<Appliance>()

    fun appliance(name: String, consumption: Double, x: ApplianceBuilder.() -> Unit){
        val xxx = ApplianceBuilder(name, consumption)
        x.invoke(xxx)


        appliances.add(xxx.build())
    }

    fun build() = Season(months, appliances)
}

class SimulationBuilder{
    val blocks = mutableListOf<Season>()

    fun season(vararg months: Int, x: BlockBuilder.() -> Unit){
        val block =  BlockBuilder(*months)
        x.invoke(block)
        blocks.add(block.build())
    }

    fun build() = blocks
}

fun simulation(
    name: String,
    data: List<IntervalPrices>,
    step: Int,
    builder: SimulationBuilder.() -> Unit
): Simulation{
    val x = SimulationBuilder()
    builder.invoke(x)

    return Simulation(name, data, step, x.build())
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)

fun t(h: Int, m:Int) = LocalTime.of(h, m)



