package bt.veselj57.fel.cvut.cz.simulation.appliances

import bt.veselj57.fel.cvut.cz.strategies.ApplianceAction
import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import bt.veselj57.fel.cvut.cz.strategies.PowerPlan
import bt.veselj57.fel.cvut.cz.strategies.PowerStrategy
import format
import java.time.LocalDate
import java.time.LocalTime



data class Consumption(val time: LocalTime, val price: Double, val amount: Double){
    override fun toString() = "$time, \t${price.format(2)} \t${amount.format(2)} "
}


open class Appliance(
    name: String,
    val kW: Double,
    val strategies: List<PowerStrategy>
){
    val history = hashMapOf<LocalDate, MutableList<Consumption>>()

    fun process(dataset: IntervalPrices, step: Int){
        val plan = PowerPlan(dataset, step)

        strategies.forEach { it.calc(plan) }

        val ons = plan.actions.mapIndexedNotNull {
            i, a -> if (a == ApplianceAction.OFF) null else i
        }
        ons.forEach {
            val item = plan[it]
            val date = item.time.toLocalDate()
            val time = item.time.toLocalTime()
            val consumption = Consumption(
                time,
                plan.getPrice(item.position),
                kW * (plan.step.toDouble()/60)
            )

            val day = history[date]
            if (day != null)
                day.add(consumption)
            else
                history[date] = mutableListOf(consumption)
        }

    }

    fun getTotalEnergyConsumption() =  history.map { it.value.sumByDouble { it.amount } }.sum()

    fun getTotalBill() =  history.map { it.value.sumByDouble { it.amount * it.price } }.sum()
}

class ApplianceImpl(name: String, kW: Double, vararg strategies: PowerStrategy):Appliance(name, kW, strategies.toList())