package bt.veselj57.fel.cvut.cz

import java.time.LocalDate
import java.time.LocalDateTime

data class HourPrice(val time: LocalDateTime, var price: Double){

    override fun toString(): String = price.toString()
}

