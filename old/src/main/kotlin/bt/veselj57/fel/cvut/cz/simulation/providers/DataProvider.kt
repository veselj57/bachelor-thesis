package bt.veselj57.fel.cvut.cz.simulation.providers

import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import java.time.LocalDate

interface DataProvider{
    fun getDataset(from: LocalDate, to: LocalDate, offset: Int): List<IntervalPrices>
}