package bt.veselj57.fel.cvut.cz.strategies

import bt.veselj57.fel.cvut.cz.HourPrice
import java.time.LocalDateTime
import java.time.LocalTime


class SimpleStrategy(
    override val from: LocalTime,
    override val to: LocalTime
): PowerStrategy() {


    override fun calc(plan: PowerPlan) {
        val interval = plan.getIndeces(from, to)

        for (i in interval){
            plan[i] = ApplianceAction.ON
        }
    }
}
