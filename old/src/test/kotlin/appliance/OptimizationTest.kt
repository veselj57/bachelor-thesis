package appliance

import bt.veselj57.fel.cvut.cz.strategies.ApplianceAction
import bt.veselj57.fel.cvut.cz.strategies.BlockOptimizer
import bt.veselj57.fel.cvut.cz.strategies.IntervalPrices
import bt.veselj57.fel.cvut.cz.strategies.PowerPlan
import org.junit.Test
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFalse


class OptimizationTest {

    @Test
    fun BlockOptimizer_test(){
        val prices = Array(24){10.0}.apply {arrayOf(1, 2, 3).forEach { this[it] = 1.0  } }

        val from = LocalDateTime.of(2018, 1, 1, 0, 0)
        val to = LocalDateTime.of(2018, 1, 1, 23, 30)

        val data = IntervalPrices(from, to, prices)

        val plan = PowerPlan(data, 30)

        assertFalse(plan.actions.any { it == ApplianceAction.ON }, "All actions in plan must be off")

        BlockOptimizer(from.toLocalTime(), to.toLocalTime(), 6, 1).calc(plan)

        assertEquals(plan[1].action, ApplianceAction.ON, "")
        assertEquals(plan[2].action, ApplianceAction.ON, "")
        assertEquals(plan[3].action, ApplianceAction.ON, "")


    }


}